﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Formulas
{
	/// <summary>
	/// This class contains formulas based on bermudan equations
	///
	/// Converted to C# from "Financial Numerical Recipes in C" by:
	/// Bernt Arne Odegaard
	/// http://finance.bi.no/~bernt/gcc_prog/index.html
	/// </summary>
	public class BermudanFormulas
	{
		/// <summary>
		/// Bermudan Option (Call) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="q">artificial "probability"</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="potentialExerciseTimes">Array of potential exercise times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallBermudanBinomial(double S, double K, double r,
			double q, double sigma, double time, double[] potentialExerciseTimes, int steps)
		{
			double delta_t = time/steps;
			double R = Math.Exp(r*delta_t);            
			double Rinv = 1.0/R;                  
			double u = Math.Exp(sigma*Math.Sqrt(delta_t));  
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (Math.Exp((r-q)*(delta_t))-d)/(u-d);
			double p_down = 1.0-p_up;
			double[] prices = new double[steps+1];       
			double[] call_values = new double[steps+1];  

			List<int> potential_exercise_steps = new List<int>(); // create list of steps at which exercise may happen
			for (int i=0;i<potentialExerciseTimes.Count(); ++i)
			{
				double t = potentialExerciseTimes[i];
				if ( (t>0.0)&&(t<time) ) 
				{
					potential_exercise_steps.Add((int)(t/delta_t));
				}
			}
			prices[0] = S*Math.Pow(d, steps);  // fill in the endnodes.
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1];
			for (int i=0; i<=steps; ++i) 
				call_values[i] = Math.Max(0.0, (prices[i]-K));
			for (int step=steps-1; step>=0; --step) 
			{
				bool check_exercise_this_step=false;
				for (int j=0;j<potential_exercise_steps.Count();++j)
				{
					if (step==potential_exercise_steps[j]) 
						check_exercise_this_step=true; 
				}
				for (int i=0; i<=step; ++i) 
				{
					call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
					prices[i] = d*prices[i+1];
					if (check_exercise_this_step) 
						call_values[i] = Math.Max(call_values[i],prices[i]-K); 
				}
			}
			return call_values[0];
		}

		/// <summary>
		/// Bermudan Option (Put) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="X">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="q">artificial "probability"</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="potentialExerciseTimes">Array of potential exercise times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutBermudanBinomial(double S, double X, double r,
			double q, double sigma, double time, double[] potentialExerciseTimes, int steps)
		{
			double delta_t=time/steps;
			double R = Math.Exp(r*delta_t);       
			double Rinv = 1.0/R;                
			double u = Math.Exp(sigma*Math.Sqrt(delta_t));
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (Math.Exp((r-q)*delta_t)-d)/(u-d);
			double p_down = 1.0-p_up; 
			double[] prices = new double[steps+1];     
			double[] put_values = new double[steps+1]; 

			List<int> potential_exercise_steps = new List<int>(); // create list of steps at which exercise may happen
			for (int i=0;i<potentialExerciseTimes.Count();++i)
			{
				double t = potentialExerciseTimes[i];
				if ( (t>0.0)&&(t<time) ) 
				{
					potential_exercise_steps.Add((int)(t/delta_t));
				}
			}

			prices[0] = S*Math.Pow(d, steps);  // fill in the endnodes.
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1];
			for (int i=0; i<=steps; ++i) 
				put_values[i] = Math.Max(0.0, (X-prices[i])); // put payoffs at maturity
			for (int step=steps-1; step>=0; --step) 
			{
				bool check_exercise_this_step=false;
				for (int j=0;j<potential_exercise_steps.Count();++j)
				{
					if (step==potential_exercise_steps[j])
						check_exercise_this_step=true; 
				}
				for (int i=0; i<=step; ++i) 
				{
					put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
					prices[i] = d*prices[i+1]; 
					if (check_exercise_this_step) 
						put_values[i] = Math.Max(put_values[i],X-prices[i]);
				}
			}
			return put_values[0];
		}
	}
}
