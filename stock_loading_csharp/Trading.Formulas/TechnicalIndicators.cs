﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trading.Data;

namespace Trading.Formulas
{
	/// <summary>
	/// Technical Indicator methods
	/// </summary>
	public class TechnicalIndicators
	{

		/// <summary>
		/// Returns a pivot point object
		/// </summary>
		/// <param name="high">High price for a specific period</param>
		/// <param name="low">Low price for a specific period</param>
		/// <param name="close">Closing price for a specific period</param>
		/// <returns>A pivot Point object</returns>
		public static PivotPoint GetPivotPoint(double high, double low, double close) 
		{
			var pivotPoint = new PivotPoint();
		
			var range = high - low;
			var p = (high + low + close) / 3;
		
			pivotPoint.P = p;
			pivotPoint.R1 = (p * 2) - low;
			pivotPoint.R2 = p + (range);
			pivotPoint.R3 = p + (range * 2);
			pivotPoint.S1 = (p * 2) - high;
			pivotPoint.S2 = p - (range);
			pivotPoint.S3 = p - (range * 2);
		
			return pivotPoint;
		}

		/// <summary>
		/// Returns a pivot point object
		/// </summary>
		/// <param name="high">High price for a specific period</param>
		/// <param name="low">Low price for a specific period</param>
		/// <param name="close">Closing price for a specific period</param>
		/// <param name="open">The opening price for a specific period</param>
		/// <returns>A pivot Point object.</returns>
		public static PivotPoint GetPivotPoint(double high, double low, double close, double open) 
		{
			var pivotPoint = new PivotPoint();
		
			double range = high - low;
			double p = (high + low + close + open) / 4;
		
			pivotPoint.P = p;
			pivotPoint.R1 = (p * 2) - low;
			pivotPoint.R2 = p + (range);
			pivotPoint.R3 = p + (range * 2);
			pivotPoint.S1 = (p * 2) - high;
			pivotPoint.S2 = p - (range);
			pivotPoint.S3 = p - (range * 2);
		
			return pivotPoint;
		}

		/// <summary>
		/// Return the Simple moving average for a particular period
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stockPrices' where the simple moving average applies</param>
		/// <param name="period">The period of this moving average, for example 20 day moving average.</param>
		/// <returns>Returns the simple moving average</returns>
		public static double GetSMA(List<StockPrice> stockPrices, int idx, int period) 
		{	
			var maArray = new List<StockPrice>();
		
			if (idx >= (period - 1)) 
			{
				int startMaArray = idx - (period - 1);
				maArray = stockPrices.GetRange(startMaArray, period);
			
				double sum = 0.0;
				foreach (var sp in maArray) 
				{
					sum += (double)sp.closing_price;
				}
			
				double ma = sum / period;
				return ma;
			} 
			else 
			{
				return 0.0;
			}
		
		}

		/// <summary>
		/// Return the Exponential moving average for a particular period
		/// </summary>
		/// <param name="stockPrices">Return the Exponential moving average for a particular period</param>
		/// <param name="idx">The index of the 'stockPrices' where the simple moving average applies</param>
		/// <param name="period">The period of this moving average, for example 20 day moving average.</param>
		/// <param name="previousEMA">The previous EMA that was calculated.</param>
		/// <returns>Returns the simple moving average.</returns>
		public static double GetEMA(List<StockPrice> stockPrices, int idx, int period, double previousEMA) 
		{	
			double smoothingConstant = 2.0f / (period +1);
			List<StockPrice> maArray = new List<StockPrice>();
		
			if (idx == (period - 1)) 
			{
				int startMaArray = idx - (period -1);
				maArray = stockPrices.GetRange(startMaArray, period);
			
				double sum = 0.0;
				foreach (StockPrice sp in maArray) 
				{
					sum += (double) sp.closing_price;
				}
			
				double ema = sum / period;
				return ema;
			} 
			else if (idx > (period-1) && previousEMA != 0) 
			{
				double currentClose = (double)stockPrices.ElementAt(idx).closing_price;
				double ema = ( (smoothingConstant * (currentClose - previousEMA)) + previousEMA );
				return ema;
			} 
			else 
			{
				return 0;
			}
		
		}

		/// <summary>
		/// Returns the Moving Average Convergence/Divergence (MACD)
		/// </summary>
		/// <param name="ema12">Exponential 12 day moving average</param>
		/// <param name="ema26">Exponential 26 day moving average</param>
		/// <returns>Returns the MACD value</returns>
		public static double GetMACD(double ema12, double ema26) 
		{
			return (ema12 - ema26);
		}

		/// <summary>
		/// Return the MACD Exponential moving average for a particular period
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stock_prices_array' where the simple moving average applies</param>
		/// <param name="period">The period of this moving average, for example 9 day moving average.</param>
		/// <param name="previousMACDEMA">The previous MACD EMA that was calculated.</param>
		/// <returns>Returns the simple moving average.</returns>
		public static double GetMACDEMA(List<StockPrice> stockPrices, int idx, int period, double previousMACDEMA) 
		{

			double smoothingConstant = 2.0f / (period +1);
			List<StockPrice> maArray = new List<StockPrice>();
		
			if (idx == (period - 1)) 
			{
				int startMaArray = idx - (period -1);
				maArray = stockPrices.GetRange(startMaArray, period);
			
				double sum = 0.0;
				foreach (StockPrice sp in maArray) 
				{
					sum += (double)sp.macd_12_26;
				}
			
				double ema = sum / period;
				return ema;
			} 
			else if (idx > (period-1)) 
			{
				double currentClose = (double) stockPrices.ElementAt(idx).macd_12_26;
				double ema = ( (smoothingConstant * (currentClose - previousMACDEMA)) + previousMACDEMA );			
				return ema;
			} 
			else 
			{
				return 0;
			}
		
		}

		/// <summary>
		/// Returns the Moving Average Convergence/Divergence (MACD) Histogram
		/// </summary>
		/// <param name="macd">The MACD value</param>
		/// <param name="macdEma9">Exponential 9 day moving average</param>
		/// <returns>Returns the MACD Histogram value</returns>
		public static double GetMACDHistogram(double macd, double macdEma9) 
		{
			return (macd - macdEma9);
		}

		/// <summary>
		/// Return the Volume Simple moving average for a particular period
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stock_prices_array' where the simple moving average applies</param>
		/// <param name="period">The period of this moving average, for example 20 day moving average.</param>
		/// <returns>Returns the simple moving average for volumes.</returns>
		public static int GetVolSMA(List<StockPrice> stockPrices, int idx, int period) 
		{
			List<StockPrice> maArray = new List<StockPrice>();
		
			if (idx >= (period - 1)) 
			{
				int startMaArray = idx- (period -1);
				maArray = stockPrices.GetRange(startMaArray, period);
			
				int sum = 0;
				foreach (StockPrice sp in maArray) 
				{
					sum += (int)sp.volume;
				}
			
				int ma = sum / period;
				return ma;
			} 
			else 
			{
				return 0;
			}
		}

		/// <summary>
		/// Returns the Percentage Price Oscillator (PPO)
		/// </summary>
		/// <param name="ema12">Exponential 12 day moving average</param>
		/// <param name="ema26">Exponential 26 day moving average</param>
		/// <returns>Returns the PPO value</returns>
		public static double GetPPO(double ema12, double ema26) 
		{
			if (ema26 == 0) 
			{
				return 0;
			}
			return (ema12 - ema26) / ema26;
		}

		/// <summary>
		/// Return the PPO Exponential moving average for a particular period
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stockPrices' where the simple moving average applies</param>
		/// <param name="period">The period of this moving average, for example 9 day moving average.</param>
		/// <param name="previousPPOEMA">The previous PPO EMA that was calculated.</param>
		/// <returns>Returns the simple moving average.</returns>
		public static double GetPPOEMA(List<StockPrice> stockPrices, int idx, int period, double previousPPOEMA) 
		{		
			double smoothingConstant = 2.0f / (period +1);
			List<StockPrice> maArray = new List<StockPrice>();
		
			if (idx == (period - 1)) 
			{
				int startMaArray = idx- (period -1);
				maArray = stockPrices.GetRange(startMaArray, period);
			
				double sum = 0.0;
				foreach (StockPrice sp in maArray) 
				{
					sum += (double)sp.ppo_12_26;
				}
			
				double ema = sum / period;
				return ema;
			} 
			else if (idx > (period-1)) 
			{
				double currentClose = (double)stockPrices.ElementAt(idx).ppo_12_26;
				double ema = ( (smoothingConstant * (currentClose - previousPPOEMA)) + previousPPOEMA );
				return ema;
			} 
			else 
			{
				return 0;
			}
		
		}

		/// <summary>
		/// Returns the Percentage Price Oscillator (PPO) Histogram
		/// </summary>
		/// <param name="ppo">The PPO value</param>
		/// <param name="ppoEma9">Exponential 9 day moving average</param>
		/// <returns>Returns the PPO Histogram value</returns>
		public static double GetPPOHistogram(double ppo, double ppoEma9) 
		{
			return (ppo - ppoEma9);
		}

		/// <summary>
		/// Returns the Relative Strength Index (RSI)
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stockPrices' of this RSI value</param>
		/// <param name="period">The period of this RSI calculation, for example 14.</param>
		/// <returns>Returns the RSI value</returns>
		public static double GetRSI(List<StockPrice> stockPrices, int idx, int period = 14) 
		{
		
			if (idx >= period) 
			{
				/*
				 * The RSI of the first calculation is different than subsequent calculations
				 * The first calculation of RSI begins when you have a number of closing prices equal to the
				 * period you're looking at.  In the default case, after 14 closing prices (Actually this will
				 * be after 15 closing prices, because you have to compare the first closing price with something
				 * to determine if there was a gain or loss.
				 */
			
				List<StockPrice> gainLossList = stockPrices.GetRange(1, period);
				List<double> gains = new List<double>();
				List<double> losses = new List<double>();
			
				double previousPrice = (double)stockPrices.ElementAt(0).closing_price;
				double previousAvgGain = 0;
				double previousAvgLoss = 0;
			
				for (int x = 0; x < gainLossList.Count; x++) 
				{
					StockPrice stockPrice = gainLossList.ElementAt(x);
					double priceChange = (double)stockPrice.closing_price - previousPrice;
					if (priceChange < 0) 
					{
						losses.Add(Math.Abs(priceChange));
					} 
					else 
					{
						gains.Add(priceChange);
					}
					// Set this price as the new previous_price for the next iteration
					previousPrice = (double)stockPrice.closing_price; 	
				} // end gainLostList...
			
				double sum = 0.0;
				foreach (double g in gains) 
				{
					sum += g;
				}
				double avgGain = sum / period;
			
				sum = 0.0;
				foreach (double l in losses) 
				{
					sum += l;
				}
				double avgLoss = sum / period;
			
				previousAvgGain = avgGain;
				previousAvgLoss = avgLoss;
			
				double firstRS = avgGain / avgLoss;
				double firstRSI = 0;
			
				if (avgLoss == 0) 
				{
					firstRSI = 0;
				} 
				else 
				{
					firstRSI = (100 - (100 / (1 + firstRS) ));
				}
			
				if (idx == period) 
				{
					return firstRSI;
				}
			
				// Now that the first RSI value has been established, the running RSI calculation can begin
			
				for (int x=0; x<stockPrices.Count; x++) 
				{
					if (x<=period) 
					{
						continue;
					}
					StockPrice stockPrice = stockPrices.ElementAt(x);
					double priceChange = (double)stockPrice.closing_price - previousPrice;
				
					/*
					 *  Now that the price change has been calculated, set the current price 
					 *  to the previous price for the next iteration.
					 */
					previousPrice = (double)stockPrice.closing_price;
				
					double gain = 0;
					double loss = 0;
					if (priceChange < 0) 
					{
						loss = Math.Abs(priceChange); // losses are represented as positive numbers
					} else {
						gain = priceChange;
					}
				
					avgGain = ((previousAvgGain * (period-1)) + gain) / period;
					avgLoss = ((previousAvgLoss * (period-1)) + loss) / period;
				
					/*
					 * Now that the average gain/loss has been established, set the 
					 * current gain/loss to the previous_avg_gain/loss for the next iteration.
					 */
					previousAvgGain = avgGain;
					previousAvgLoss = avgLoss;
				
					double rs = avgGain / avgLoss;
					double rsi = 0;
					if (avgLoss == 0) 
					{
						rsi = 100;
					} 
					else 
					{
						rsi = (100 - (100 / (1 + rs) ));
					}
				
					if (idx == x) 
					{
					  return rsi;
					}
				} // end stockPrices...
				return 0;  // If for some reason a value hasn't been returned, set RSI to 0
			} 
			else 
			{  // end idx >= period...
				return 0;
			}
		}

		/// <summary>
		/// Returns the Stochastics for a specific stock price in a series
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stockPrices' of this RSI value</param>
		/// <param name="kPeriod">The period for %K (example 10)</param>
		/// <param name="dPeriod">The period for %D (example 3)</param>
		/// <param name="k">Return value for k</param>
		/// <param name="d">Return value for d</param>
		/// <param name="kSlow">Return value for k (slow)</param>
		/// <param name="dSlow">Return value for d (slow)</param>
		public static void GetStochastics(List<StockPrice> stockPrices, int idx, int kPeriod, int dPeriod,
			out double k, out double d, out double kSlow, out double dSlow) 
		{
			// Set defaults if necessary.
			if (kPeriod == 0) 
				kPeriod = 10;
			if (dPeriod == 0)
				dPeriod = 3;
		
			if (idx >= (kPeriod -1) ) {
				/*
				 * The array below will keep track of all numerators and denominators used
				 *  to calculate %K, these values will then be used to calculate %D
				 */
			
				List<double> fastKList = new List<double>();
			
				// This array keeps track of slow %K to later calculate slow %D
				List<double> slowKList = new List<double>();
			
				for (int x =0; x<stockPrices.Count; x++) {
					if (x>= (kPeriod - 1)) {
						StockPrice stockPrice = stockPrices.ElementAt(x);
					
						int startPeriodList = x - (kPeriod - 1);
						List<StockPrice> periodList = stockPrices.GetRange(startPeriodList, kPeriod);
						double maxHigh = (double)periodList.Max(p => p.high_price);
						double minLow = (double)periodList.Min(p => p.low_price);
					
						double num = (double)stockPrice.closing_price - minLow;
						double den = maxHigh - minLow;
						double _k = 0;
						if ( num == 0 ) 
						{
							_k = 0;
						} 
						else 
						{
							_k = 100 * (num / den);
						}
						fastKList.Add(_k);
					
						double _d = 0;
					
						// If there is enough old data, calculate %D
						if (fastKList.Count >= dPeriod) 
						{
							int tmpFastKStart = fastKList.Count - dPeriod;
							List<double> fastKListTrimmed = fastKList.GetRange(tmpFastKStart, dPeriod);
						
							double sum = 0;
							foreach (double f in fastKListTrimmed) 
							{
								sum += f;
							}       
							if (sum == 0)
							{
								_d = 0;
							} 
							else 
							{
								_d = sum / dPeriod;
							}
						} 
						else 
						{
							_d = 0;
						}
					
						// Calculate Stochastics Slow
						// # %D (Fast) is the %K slow
						double _kSlow = _d;
						if (_kSlow != 0) 
						{
							slowKList.Add(_kSlow);
						}

						double _dSlow = 0;
					
						// If there is enough old data, calculate %D (slow)
						if (slowKList.Count == dPeriod) 
						{ 
							double sum = 0;
							foreach (double f in slowKList) 
							{
								sum += f;
							}
							if (sum == 0) 
							{
								_dSlow = 0;
							} 
							else 
							{
								_dSlow = sum / dPeriod;
							}
						} 
						else if (slowKList.Count > dPeriod) 
						{
							int tmpSlowKStart = slowKList.Count - dPeriod;
							List<double> tmpSlowKList = slowKList.GetRange(tmpSlowKStart, dPeriod);
							double sum = 0;
							foreach (double f in tmpSlowKList) 
							{
								sum += f;
							}
							if (sum == 0) 
							{
								_dSlow = 0;
							} 
							else 
							{
								_dSlow = sum / dPeriod; 
							}
						} 
						else 
						{
							_dSlow = 0; 
						}
					
						// If this is the index we want, return all the values
						if (idx ==x) 
						{             	
							k = _k;
							d = _d;
							kSlow = _kSlow;
							dSlow = _dSlow;
							return;
						}
					
					} //end x> (kPeriod -1...
				} //end for stockPrices...
			} //end if(idx > kPeriod...	
				
			k = 0;
			d = 0;
			kSlow = 0;
			dSlow = 0;
		}

		/// <summary>
		/// Returns the Average True Range
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stockPrices' of this ATR value</param>
		/// <param name="previous_atr">The previous ATR value.</param>
		/// <param name="period">The period for the ATR</param>
		/// <returns>double Returns the Average True Range</returns>
		public static double GetATR(List<StockPrice> stockPrices, int idx, double previous_atr, int period = 14) 
		{
			double prev_close = 0;
			List<double> daily_tr_arr = new List<double>();
		
			if (idx >= period -1) 
			{
				prev_close = 0;
				daily_tr_arr = new List<double>();
				for (int x=0; x<stockPrices.Count; x++) 
				{
					double daily_tr = 0;
				
					// For the first entry, the Daily TR is the 'High' - 'Low'
					if (x==0) 
					{
						daily_tr = (double)(stockPrices.ElementAt(x).high_price - stockPrices.ElementAt(x).low_price);
					} 
					else 
					{
						double high_low = (double)(stockPrices.ElementAt(x).high_price - stockPrices.ElementAt(x).low_price);
						double high_prev_close = Math.Abs((double)stockPrices.ElementAt(x).high_price - prev_close);
						double low_prev_close = Math.Abs((double)stockPrices.ElementAt(x).low_price - prev_close);
						double[] max_tmp_arr = new double[] {high_low, high_prev_close, low_prev_close};
						daily_tr = max_tmp_arr.Max();
					}
					// Make the prev close, the current prev close and add the TR to the TR array.
					prev_close = (double)stockPrices.ElementAt(x).closing_price;
					daily_tr_arr.Add(daily_tr);
			  
					// Slice off the daily_tr_arr to the period and generate an average
					// if the index is the period value, otherwise, use the smoothed out average.       
					double atr = 0;
					if (idx == period - 1 && x == idx) 
					{
						int start_tmp_daily_tr_arr = daily_tr_arr.Count - period;
						List<double> tmp_daily_tr_arr = daily_tr_arr.GetRange(start_tmp_daily_tr_arr, period);        
						double sum = 0.0;
						foreach (double dailyTr in tmp_daily_tr_arr) 
						{
							sum += dailyTr;
						}
						atr = sum / period;
						return atr;
					} 
					else if (idx > period - 1 && x == idx) 
					{       
					  atr = (previous_atr * (period -1) + daily_tr) / period;
					  return atr;
					} // (if (idx == x)...          
				} // for (int x=0; x<stockPrices...
			} 
			else 
			{
				return 0.0;
			}
			return 0.0;
		}
	
		/// <summary>
		/// Calculates historic volatility which essentially uses a SMA.
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stockPrices' of this value</param>
		/// <param name="period">The period for the historic volatility</param>
		/// <param name="tradingDays">Trading days to use for the volatility calculation. (252 for a year, etc.)</param>
		/// <returns></returns>
		public static double GetHistoricalVolatility(List<StockPrice> stockPrices, int idx, int period = 20, int tradingDays = 252) 
		{
			if (idx >= period)  // User 'period', instead of (period -1) since the first calc is period+1
			{ 
				List<double> priceChanges = new List<double>();
				double lastPrice = 0;
				for(int x=0; x<stockPrices.Count; x++) {
					if (x==0) {
						priceChanges.Add(0.0);
						lastPrice = (double)stockPrices.ElementAt(x).closing_price;
					} else {
						double priceChange = Math.Log((double)stockPrices.ElementAt(x).closing_price / lastPrice);
						priceChanges.Add(priceChange);
						lastPrice = (double)stockPrices.ElementAt(x).closing_price;
					}
				}
				// Grab an array of price changes over the selected period
				int tmp_price_changes_st = idx - (period -1);
				List<double> price_changes = priceChanges.GetRange(tmp_price_changes_st, period);
				double stdDevTmp = CalculateStdDev(price_changes);
				if (stdDevTmp == Double.NaN)
					return 0.0;
				double retTemp = stdDevTmp * Math.Sqrt(tradingDays);
				return (double)retTemp;
			} else {
				return 0.0;
			}
		}

		/// <summary>
		/// Exponential weighted moving average (EWMA) volatility
		/// This calculation was derived from a spreadsheet at Bionic Turtle
		/// (http://www.bionicturtle.com/)
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'stock_prices_array' where this calculation applies</param>
		/// <param name="tradingDays">The # of days used in the calculation (252 for annualized volatility)</param>
		/// <param name="lambdaConst">The lambda value used for the calculation. Defaults to the Risk Metrics value of 94%</param>
		/// <param name="arraySize">The number of days to go back in the history of prices to make this calculation.</param>
		/// <returns>Volatility of the exponentially weighted moving average in decimal form. Multiply by 100 to get a percentage.</returns>
		public static double GetEWMAVolatility(List<StockPrice> stockPrices, int idx, int tradingDays = 252,
			double lambdaConst = 0.94F, int arraySize = 252)
		{
			var adjArraySize = arraySize;
			if (stockPrices.Count < arraySize)
				adjArraySize = stockPrices.Count;

			var periodReturns = new List<double>();
			var periodReturnsSquared = new List<double>(); // Returns squared, which is the same as simple volatility

			// Create a subset of the prices based on the arraySize
			var tmpPriceSt = idx - (adjArraySize - 1);
			var tmpStockPrices = stockPrices.GetRange(tmpPriceSt, adjArraySize);

			double lastPrice = 0;
			for (int x = 0; x < tmpStockPrices.Count; x++)
			{
				if (x == 0)
				{
					periodReturns.Add(0);
					periodReturnsSquared.Add(0);
					lastPrice = (double)tmpStockPrices.ElementAt(x).closing_price;
				}
				else
				{
					var priceChange = Math.Log((double)tmpStockPrices.ElementAt(x).closing_price / lastPrice);
					periodReturns.Add(priceChange);
					periodReturnsSquared.Add(Math.Pow(priceChange, 2));
					lastPrice = (double)tmpStockPrices.ElementAt(x).closing_price;
				}
			}

			var weightVars = new List<double>(); // Weighted variances for day 'N'
			var weightVarsPrev = new List<double>(); // Weighted variances for day 'N-1'

			double weight = 0; // The weight for day 'N'
			double weightPrev = 0; // The weight for day 'N-1'

			// Use a reverse loop to run through the prices from most recent to oldest
			for (int i = tmpStockPrices.Count; i > 0; i-- )
			{
				if (i == (tmpStockPrices.Count() - 2)) // Weight for day 'N'
				{
					weight = 1.0F - lambdaConst;
					weightVars.Add((double)periodReturnsSquared.ElementAt(i) * weight);
				}
				else if (i == (tmpStockPrices.Count() - 3)) // Weight for day 'N-1'
				{
					weightPrev = 1.0F - lambdaConst;
					weight = weight * lambdaConst;
					weightVars.Add((double)periodReturnsSquared.ElementAt(i) * weight);
					weightVarsPrev.Add((double)periodReturnsSquared.ElementAt(i) * weightPrev);
				}
				else if (i < tmpStockPrices.Count() - 3)
				{
					weight = weight * lambdaConst;
					weightPrev = weightPrev * lambdaConst;
					weightVars.Add((double)periodReturnsSquared.ElementAt(i) * weight);
					weightVarsPrev.Add((double)periodReturnsSquared.ElementAt(i) * weightPrev);
				}
			}
			var sumVars = weightVars.Sum();
			var sumVarsPrev = weightVarsPrev.Sum();
			var ewma = lambdaConst * sumVarsPrev + (1 - lambdaConst) * periodReturnsSquared.ElementAt(adjArraySize - 2);
			return Math.Sqrt(ewma) * Math.Sqrt(tradingDays);
		}

		/// <summary>
		/// Calculates volatility using the GARCH(1,1) formula.
		/// This calculation was derived from a spreadsheet at Bionic Turtle
		/// (http://www.bionicturtle.com/)
		/// (NOTE: Gamma is determined using:  1 - alpha - beta)
		/// </summary>
		/// <param name="stockPrices">An array of StockPrice objects</param>
		/// <param name="idx">The index of the 'StockPrices' where this calculation applies</param>
		/// <param name="longRunVariance">The long run variance for this series of prices</param>
		/// <param name="alpha">The Alpha weight used for this calculation</param>
		/// <param name="beta">The Beta weight used for this calculation</param>
		/// <param name="lambdaConst">The lambda value used for the calculation. Defaults to the Risk Metrics value of 94%</param>
		/// <param name="tradingDays">The # of days used in the calculation (252 for annualized volatility)</param>
		/// <param name="arraySize">The number of days to go back in the history of prices to make this calculation.</param>
		/// <returns>Volatility of the GARCH(1,1) formula in decimal form. Multiply by 100 to get a percentage.</returns>
		public static double GetGarch1_1Volatility(List<StockPrice> stockPrices, int idx, double longRunVariance,
			double alpha, double beta, double lambdaConst = 0.94F, int tradingDays = 252, int arraySize = 252)
		{
			var adjArraySize = arraySize;
			if (stockPrices.Count < arraySize)
				adjArraySize = stockPrices.Count;

			// The three weights Alpha + Beta + Gamma can not be greater than 1
			// Throw an exception if this occurs.
			if (alpha + beta > 1.0)
				throw new Exception("The weights Alpha + Beta + Gamma should not be greater than 1");
			double gamma = 1 - alpha - beta;

			var periodReturns = new List<double>();
			var periodReturnsSquared = new List<double>(); // Returns squared, which is the same as simple volatility

			// Create a subset of the prices based on the arraySize
			var tmpPriceSt = idx - (adjArraySize - 1);
			var tmpStockPrices = stockPrices.GetRange(tmpPriceSt, adjArraySize);

			double lastPrice = 0;
			for (int x = 0; x < tmpStockPrices.Count; x++)
			{
				if (x == 0)
				{
					periodReturns.Add(0);
					periodReturnsSquared.Add(0);
					lastPrice = (double)tmpStockPrices.ElementAt(x).closing_price;
				}
				else
				{
					var priceChange = Math.Log((double)tmpStockPrices.ElementAt(x).closing_price / lastPrice);
					periodReturns.Add(priceChange);
					periodReturnsSquared.Add(Math.Pow(priceChange, 2));
					lastPrice = (double)tmpStockPrices.ElementAt(x).closing_price;
				}
			}

			var weightVars = new List<double>(); // Weighted variances for day 'N'
			var weightVarsPrev = new List<double>(); // Weighted variances for day 'N-1'

			double weight = 0; // The weight for day 'N'
			double weightPrev = 0; // The weight for day 'N-1'

			// Use a reverse loop to run through the prices from most recent to oldest
			for (int i = tmpStockPrices.Count; i > 0; i--)
			{
				if (i == (tmpStockPrices.Count() - 2)) // Weight for day 'N'
				{
					weight = 1.0F - lambdaConst;
					weightVars.Add((double)periodReturnsSquared.ElementAt(i) * weight);
				}
				else if (i == (tmpStockPrices.Count() - 3)) // Weight for day 'N-1'
				{
					weightPrev = 1.0F - lambdaConst;
					weight = weight * lambdaConst;
					weightVars.Add((double)periodReturnsSquared.ElementAt(i) * weight);
					weightVarsPrev.Add((double)periodReturnsSquared.ElementAt(i) * weightPrev);
				}
				else if (i < tmpStockPrices.Count() - 3)
				{
					weight = weight * lambdaConst;
					weightPrev = weightPrev * lambdaConst;
					weightVars.Add((double)periodReturnsSquared.ElementAt(i) * weight);
					weightVarsPrev.Add((double)periodReturnsSquared.ElementAt(i) * weightPrev);
				}
			}
			var sumVars = weightVars.Sum();
			var sumVarsPrev = weightVarsPrev.Sum();
			var garch = (longRunVariance * gamma) + (alpha *
				periodReturnsSquared.ElementAt(adjArraySize - 2)) + (beta * sumVarsPrev);
			return Math.Sqrt(garch) * Math.Sqrt(tradingDays);
		}

		/// <summary>
		/// Calculates the standard deviation for a list of double values.
		/// </summary>
		/// <param name="values">List of values used in the standard devation</param>
		/// <returns>Standard Deviation</returns>
		private static double CalculateStdDev(IEnumerable<double> values)
		{
			double ret = 0;
			if (values.Count() > 0)
			{
				//Compute the Average      
				double avg = values.Average();
				//Perform the Sum of (value-avg)_2_2      
				double sum = values.Sum(d => Math.Pow(d - avg, 2));
				//Put it all together      
				ret = Math.Sqrt((sum) / (values.Count() - 1));
			}
			return ret;
		}
	}
}
