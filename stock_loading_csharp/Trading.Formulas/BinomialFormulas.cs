﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Formulas
{
	/// <summary>
	/// This class contains formulas based on binomial equations
	///
	/// Converted to C# from "Financial Numerical Recipes in C" by:
	/// Bernt Arne Odegaard
	/// http://finance.bi.no/~bernt/gcc_prog/index.html
	/// </summary>
	public class BinomialFormulas
	{
		/// <summary>
		/// American Option (Call) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallAmericanBinomial(double S, double K, double r, 
			double sigma, double t, int steps)
		{
			double R = Math.Exp(r*(t/steps));         
			double Rinv = 1.0/R;                 
			double u = Math.Exp(sigma*Math.Sqrt(t/steps)); 
			double d = 1.0/u;
			double p_up = (R-d)/(u-d);
			double p_down = 1.0-p_up;

			double[] prices = new double[steps+1];       // price of underlying
			prices[0] = S*Math.Pow(d, steps);  // fill in the endnodes.
			double uu = u*u;
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1];

			double[] call_values = new double[steps+1];       // value of corresponding call 
			for (int i=0; i<=steps; ++i) 
				call_values[i] = Math.Max(0.0, (prices[i]-K)); // call payoffs at maturity

			for (int step=steps-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i) 
				{
					call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
					prices[i] = d*prices[i+1];
					call_values[i] = Math.Max(call_values[i],prices[i]-K);       // check for exercise
				}
			}
			return call_values[0];
		}

		/// <summary>
		/// American Option (Put) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutAmericanBinomial(double S, double K, double r,
			double sigma, double t, int steps)
		{
			double R = Math.Exp(r*(t/steps));            // interest rate for each step
			double Rinv = 1.0/R;                    // inverse of interest rate
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));    // up movement
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (R-d)/(u-d);
			double p_down = 1.0-p_up;
			double[] prices = new double[steps+1];       // price of underlying
			prices[0] = S*Math.Pow(d, steps);  
			for (int i=1; i<=steps; ++i) prices[i] = uu*prices[i-1];

			double[] put_values = new double[steps+1];       // value of corresponding put 
			for (int i=0; i<=steps; ++i) 
				put_values[i] = Math.Max(0.0, (K-prices[i])); // put payoffs at maturity

			for (int step=steps-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i)
				{
					put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
					prices[i] = d*prices[i+1];
					put_values[i] = Math.Max(put_values[i],(K-prices[i]));    // check for exercise
				}
			}
			return put_values[0];
		}

		/// <summary>
		/// Delta of an American Option (Call) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="noSteps">Number of steps in binomial tree</param>
		/// <returns>Delta of the option</returns>
		public static double OptionPriceDeltaAmericanCallBinomial(double S, double K, double r, 
			double sigma, double t, int noSteps)
		{
			double R = Math.Exp(r*(t/noSteps));
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(t/noSteps));
			double d = 1.0/u;
			double uu= u*u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;

			double[] prices = new double[noSteps+1];
			prices[0] = S*Math.Pow(d, noSteps);
			for (int i=1; i<=noSteps; ++i) prices[i] = uu*prices[i-1];

			double[] call_values = new double[noSteps+1];
			for (int i=0; i<=noSteps; ++i) 
				call_values[i] = Math.Max(0.0, (prices[i]-K));

			for (int CurrStep=noSteps-1 ; CurrStep>=1; --CurrStep) 
			{
				for (int i=0; i<=CurrStep; ++i)   
				{
					prices[i] = d*prices[i+1];
					call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
					call_values[i] = Math.Max(call_values[i], prices[i]-K);        // check for exercise
				}
			}
			double delta = (call_values[1]-call_values[0])/(S*u-S*d);
			return delta;
		}

		/// <summary>
		/// Delta of an American Option (Put) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="noSteps">Number of steps in binomial tree</param>
		/// <returns>Delta of the option</returns>
		public static double OptionPriceDeltaAmericanPutBinomial(double S, double K, double r,
			double sigma, double t, int noSteps)
		{
			double[] prices = new double[noSteps+1];
			double[] put_values = new double[noSteps+1];
			double R = Math.Exp(r*(t/noSteps));
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(t/noSteps));
			double d = 1.0/u;
			double uu= u*u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;
			prices[0] = S*Math.Pow(d, noSteps);
			int i;
			for (i=1; i<=noSteps; ++i) 
				prices[i] = uu*prices[i-1];
			for (i=0; i<=noSteps; ++i) 
				put_values[i] = Math.Max(0.0, (K - prices[i]));
			for (int CurrStep=noSteps-1 ; CurrStep>=1; --CurrStep) 
			{
				for (i=0; i<=CurrStep; ++i)   
				{
					prices[i] = d*prices[i+1];
					put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
					put_values[i] = Math.Max(put_values[i], K-prices[i]);        // check for exercise
				};
			}; 
			double delta = (put_values[1]-put_values[0])/(S*u-S*d);
			return delta;
		}

		/// <summary>
		/// Calculate partial derivatives for an American Option (Call) using 
		/// binomial approximations.
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="noSteps">Number of steps in binomial tree</param>
		/// <param name="delta">Output - partial derivatives with respect to (wrt) S</param>
		/// <param name="gamma">Output - second partial derivatives wrt S</param>
		/// <param name="theta">Output - partial derivatives wrt time</param>
		/// <param name="vega">Output - partial derivatives wrt sigma</param>
		/// <param name="rho">Output - partial derivatives wrt r</param>
		public static void OptionPricePartialsAmericanCallBinomial(double S, double K, double r,
			double sigma, double time, int noSteps, out double delta, out double gamma,
			out double theta, out double vega, out double rho)
		{
			double[] prices = new double[noSteps+1];
			double[] call_values = new double[noSteps+1];
			double delta_t =(time/noSteps);
			double R = Math.Exp(r*delta_t);
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(delta_t));
			double d = 1.0/u;
			double uu= u*u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;
			prices[0] = S*Math.Pow(d, noSteps);
			for (int i=1; i<=noSteps; ++i) 
				prices[i] = uu*prices[i-1];
			for (int i=0; i<=noSteps; ++i) 
				call_values[i] = Math.Max(0.0, (prices[i]-K));
			for (int CurrStep=noSteps-1; CurrStep>=2; --CurrStep) 
			{
				for (int i=0; i<=CurrStep; ++i)   
				{
					prices[i] = d*prices[i+1];
					call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
					call_values[i] = Math.Max(call_values[i], prices[i]-K);        // check for exercise
				}
			}
			double f22 = call_values[2];
			double f21 = call_values[1];
			double f20 = call_values[0];
			for (int i=0;i<=1;i++) 
			{
				prices[i] = d*prices[i+1];
				call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
				call_values[i] = Math.Max(call_values[i], prices[i]-K);        // check for exercise 
			}
			double f11 = call_values[1];
			double f10 = call_values[0];
			prices[0] = d*prices[1];
			call_values[0] = (pDown*call_values[0]+pUp*call_values[1])*Rinv;
			call_values[0] = Math.Max(call_values[0], S-K);        // check for exercise on first date
			double f00 = call_values[0];
			delta = (f11-f10)/(S*u-S*d);
			double h = 0.5 * S * ( uu - d*d);
			gamma = ( (f22-f21)/(S*(uu-1)) - (f21-f20)/(S*(1-d*d)) ) / h; 
			theta = (f21-f00) / (2*delta_t);
			double diff = 0.02;
			double tmp_sigma = sigma+diff;
			double tmp_prices = OptionPriceCallAmericanBinomial(S,K,r,tmp_sigma,time,noSteps);
			vega = (tmp_prices-f00)/diff; 
			diff = 0.05;
			double tmp_r = r+diff;
			tmp_prices = OptionPriceCallAmericanBinomial(S, K, tmp_r, sigma, time, noSteps);
			rho = (tmp_prices-f00)/diff;
		}

		/// <summary>
		/// Calculate partial derivatives for an American Option (Put) using 
		/// binomial approximations.
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="noSteps">Number of steps in binomial tree</param>
		/// <param name="delta">Output - partial derivatives with respect to (wrt) S</param>
		/// <param name="gamma">Output - second partial derivatives wrt S</param>
		/// <param name="theta">Output - partial derivatives wrt time</param>
		/// <param name="vega">Output - partial derivatives wrt sigma</param>
		/// <param name="rho">Output - partial derivatives wrt r</param>
		public static void OptionPricePartialsAmericanPutBinomial(double S, double K, double r,
			double sigma, double time, int noSteps, out double delta, out double gamma,
			out double theta, out double vega, out double rho)
		{
			double[] prices = new double[noSteps+1];
			double[] put_values = new double[noSteps+1];
			double delta_t =(time/noSteps);
			double R = Math.Exp(r*delta_t);
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(delta_t));
			double d = 1.0/u;
			double uu= u*u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;
			prices[0] = S*Math.Pow(d, noSteps);
			int i;
			for (i=1; i<=noSteps; ++i) 
				prices[i] = uu*prices[i-1];
			for (i=0; i<=noSteps; ++i) 
				put_values[i] = Math.Max(0.0, (K-prices[i]));
			for (int CurrStep=noSteps-1 ; CurrStep>=2; --CurrStep)
			{
				for (i=0; i<=CurrStep; ++i)
				{
					prices[i] = d*prices[i+1];
					put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
					put_values[i] = Math.Max(put_values[i], K-prices[i]); // check for exercise
				};
			}; 
			double f22 = put_values[2];
			double f21 = put_values[1];
			double f20 = put_values[0];
			for (i=0;i<=1;i++) 
			{
				prices[i] = d*prices[i+1];
				put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
				put_values[i] = Math.Max(put_values[i], K-prices[i]); // check for exercise
			};
			double f11 = put_values[1];
			double f10 = put_values[0];
			prices[0] = d*prices[1];
			put_values[0] = (pDown*put_values[0]+pUp*put_values[1])*Rinv;
			put_values[0] = Math.Max(put_values[0], K-prices[i]); // check for exercise
			double f00 = put_values[0];
			delta = (f11-f10)/(S*(u-d));
			double h = 0.5 * S *( uu - d*d);
			gamma = ( (f22-f21)/(S*(uu-1.0)) - (f21-f20)/(S*(1.0-d*d)) ) / h;
			theta = (f21-f00) / (2*delta_t);
			double diff = 0.02;
			double tmp_sigma = sigma+diff;
			double tmp_prices = OptionPricePutAmericanBinomial(S,K,r,tmp_sigma,time,noSteps);
			vega = (tmp_prices-f00)/diff; 
			diff = 0.05;
			double tmp_r = r+diff;
			tmp_prices = OptionPricePutAmericanBinomial(S, K, tmp_r, sigma, time, noSteps);
			rho = (tmp_prices-f00)/diff; 
		}

		/// <summary>
		/// American Option (Call) for dividends with specific (discrete) dollar amounts 
		/// using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <param name="dividendTimes">Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="dividendAmounts">Array of dividend amounts for the 'dividendTimes'</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallAmericanDiscreteDividendsBinomial(double S, double K,
			double r, double sigma, double t, int steps, double[] dividendTimes, double[] dividendAmounts)
		{
			int no_dividends = dividendTimes.Count();
			if (no_dividends==0) 
				return OptionPriceCallAmericanBinomial(S,K,r,sigma,t,steps);// just do regular
			int steps_before_dividend = (int)(dividendTimes[0]/t*steps);
			double R = Math.Exp(r*(t/steps));
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));
			double d = 1.0/u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;
			double dividend_amount = dividendAmounts[0];
			double[] tmp_dividend_times = new double[no_dividends-1];  // temporaries with 
			double[] tmp_dividend_amounts = new double[no_dividends-1];  // one less dividend
			for (int i=0;i<(no_dividends-1);++i)
			{ 
				tmp_dividend_amounts[i] = dividendAmounts[i+1];
				tmp_dividend_times[i]   = dividendTimes[i+1] - dividendTimes[0];
			}
			double[] prices = new double[steps_before_dividend+1];
			double[] call_values = new double[steps_before_dividend+1];
			prices[0] = S*Math.Pow(d, steps_before_dividend);
			for (int i=1; i<=steps_before_dividend; ++i)
				prices[i] = u*u*prices[i-1];
			for (int i=0; i<=steps_before_dividend; ++i)
			{
				double value_alive = OptionPriceCallAmericanDiscreteDividendsBinomial(prices[i]-dividend_amount,
					K, r, sigma,
					t-dividendTimes[0],// time after first dividend
					steps-steps_before_dividend, 
					tmp_dividend_times.ToArray<double>(),
					tmp_dividend_amounts.ToArray<double>());
				call_values[i] = Math.Max(value_alive,(prices[i]-K));  // compare to exercising now
			}
			for (int step=steps_before_dividend-1; step>=0; --step) {
				for (int i=0; i<=step; ++i)   {
					prices[i] = d*prices[i+1];
					call_values[i] = (pDown*call_values[i]+pUp*call_values[i+1])*Rinv;
					call_values[i] = Math.Max(call_values[i], prices[i]-K); 
				}
			}
			return call_values[0];
		}

		/// <summary>
		/// American Option (Put) for dividends with specific (discrete) dollar amounts 
		/// using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <param name="dividendTimes">Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="dividendAmounts">Array of dividend amounts for the 'dividendTimes'</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutAmericanDiscreteDividendsBinomial(double S, double K,
			double r, double sigma, double t, int steps, double[] dividendTimes, double[] dividendAmounts)
		{
			// given an amount of dividend, the binomial tree does not recombine, have to 
			// start a new tree at each ex-dividend date.
			// do this recursively, at each ex dividend date, at each step, put the 
			// binomial formula starting at that point to calculate the value of the live
			// option, and compare that to the value of exercising now.

			int no_dividends = dividendTimes.Count();
			if (no_dividends == 0)               // just take the regular binomial 
				return OptionPricePutAmericanBinomial(S,K,r,sigma,t,steps);
			int steps_before_dividend = (int)(dividendTimes[0]/t*steps);
	
			double R = Math.Exp(r*(t/steps));
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));
			double uu= u*u;
			double d = 1.0/u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;
			double dividend_amount = dividendAmounts[0];
			double[] tmp_dividend_times = new double[no_dividends-1];  // temporaries with 
			double[] tmp_dividend_amounts = new double[no_dividends-1];  // one less dividend
			for (int i=0;i<no_dividends-1;++i)
			{ 
				tmp_dividend_amounts[i] = dividendAmounts[i+1];
				tmp_dividend_times[i]   = dividendTimes[i+1] - dividendTimes[0];
			}
			double[] prices = new double[steps_before_dividend+1];
			double[] put_values = new double[steps_before_dividend+1];

			prices[0] = S*Math.Pow(d, steps_before_dividend);
			for (int i=1; i<=steps_before_dividend; ++i) 
				prices[i] = uu*prices[i-1];
			for (int i=0; i<=steps_before_dividend; ++i)
			{
				double value_alive = OptionPricePutAmericanDiscreteDividendsBinomial(
					prices[i]-dividend_amount, K, r, sigma, 
					t-dividendTimes[0],               // time after first dividend
					steps-steps_before_dividend, 
					tmp_dividend_times.ToArray<double>(), 
					tmp_dividend_amounts.ToArray<double>());  
				// what is the value of keeping the option alive?  Found recursively, 
				// with one less dividend, the stock price is current value 
				// less the dividend.
				put_values[i] = Math.Max(value_alive,(K-prices[i]));  // compare to exercising now
			}
			for (int step=steps_before_dividend-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i)   
				{
					prices[i] = d*prices[i+1];
					put_values[i] = (pDown*put_values[i]+pUp*put_values[i+1])*Rinv;
					put_values[i] = Math.Max(put_values[i], K-prices[i]);         // check for exercise
				}
			}
			return put_values[0];
		}

		/// <summary>
		/// American Option (Call) with proportional dividend payments 
		/// using binomial approximations.
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="noSteps">Number of steps in binomial tree</param>
		/// <param name="dividendTimes">Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="dividendYields">Array of dividend yields for the 'dividend_times'</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallAmericanProportionalDividendsBinomial(double S, double K,
			double r, double sigma, double time, int noSteps, double[] dividendTimes, double[] dividendYields)
		{
			// note that the last dividend date should be before the expiry date, problems if dividend at terminal node
			int no_dividends=dividendTimes.Count();
			if (no_dividends == 0) 
			{
				return OptionPriceCallAmericanBinomial(S,K,r,sigma,time,noSteps); // price w/o dividends
			}
			double delta_t = time/noSteps;
			double R = Math.Exp(r*delta_t);
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(delta_t));
			double uu= u*u;
			double d = 1.0/u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;
			int[] dividend_steps = new int[no_dividends]; // when dividends are paid
			for (int i=0; i<no_dividends; ++i) 
				dividend_steps[i] = (int)(dividendTimes[i]/time*noSteps);

			double[] prices = new double[noSteps+1];
			double[] call_prices = new double[noSteps+1];
			prices[0] = S*Math.Pow(d, noSteps); // adjust downward terminal prices by dividends
			for (int i=0; i<no_dividends; ++i) 
				prices[0]*=(1.0-dividendYields[i]); 
			for (int i=1; i<=noSteps; ++i)
				prices[i] = uu*prices[i-1];  
			for (int i=0; i<=noSteps; ++i) 
				call_prices[i] = Math.Max(0.0, (prices[i]-K));

			for (int step=noSteps-1; step>=0; --step) 
			{
				for (int i=0;i<no_dividends;++i) {   // check whether dividend paid
					if (step==dividend_steps[i]) { 
						for (int j=0;j<=(step+1);++j) {
							prices[j]*=(1.0/(1.0-dividendYields[i])); 
						}
					}
				}
				for (int i=0; i<=step; ++i)   {
					call_prices[i] = (pDown*call_prices[i]+pUp*call_prices[i+1])*Rinv;
					prices[i] = d*prices[i+1];
					call_prices[i] = Math.Max(call_prices[i], prices[i]-K);         // check for exercise
				}
			}
			return call_prices[0];
		}

		/// <summary>
		/// American Option (Put) with proportional dividend payments 
		/// using binomial approximations.
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="noSteps">Number of steps in binomial tree</param>
		/// <param name="dividendTimes">Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="dividendYields">Array of dividend yields for the 'dividend_times'</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutAmericanProportionalDividendsBinomial(double S, double K,
			double r, double sigma, double time, int noSteps, double[] dividendTimes, double[] dividendYields)
		{
			// when one assume a dividend yield, the binomial tree recombines 
			// note that the last dividend date should be before the expiry date
			int no_dividends=dividendTimes.Count();
			if (no_dividends == 0)               // just take the regular binomial 
				return OptionPricePutAmericanBinomial(S,K,r,sigma,time,noSteps);
			double R = Math.Exp(r*(time/noSteps));
			double Rinv = 1.0/R;
			double u = Math.Exp(sigma*Math.Sqrt(time/noSteps));
			double uu= u*u;
			double d = 1.0/u;
			double pUp   = (R-d)/(u-d);
			double pDown = 1.0 - pUp;

			int[] dividend_steps = new int[no_dividends]; // when dividends are paid
			for (int i=0; i<no_dividends; ++i) 
			{
				dividend_steps[i] = (int)(dividendTimes[i]/time*noSteps);
			}

			double[] prices = new double[noSteps+1];
			double[] put_prices = new double[noSteps+1];
			prices[0] = S*Math.Pow(d, noSteps);
			for (int i=0; i<no_dividends; ++i) 
				prices[0]*=(1.0-dividendYields[i]); 
			for (int i=1; i<=noSteps; ++i) 
				prices[i] = uu*prices[i-1]; // terminal tree nodes
			for (int i=0; i<=noSteps; ++i) 
				put_prices[i] = Math.Max(0.0, (K-prices[i]));

			for (int step=noSteps-1; step>=0; --step) 
			{
				for (int i = 0; i < no_dividends; ++i) // check whether dividend paid
				{   
					if (step==dividend_steps[i]) 
					{ 
						for (int j=0;j<=(step+1);++j) 
						{
							prices[j]*=(1.0/(1.0-dividendYields[i])); 
						}
					}
				}
				for (int i=0; i<=step; ++i)   
				{
					prices[i] = d*prices[i+1];
					put_prices[i] = (pDown*put_prices[i]+pUp*put_prices[i+1])*Rinv;
					put_prices[i] = Math.Max(put_prices[i], K-prices[i]);         // check for exercise
				}
			}
			return put_prices[0];
		}

		/// <summary>
		/// American Option (Call) with continuous payouts using binomial 
		/// approximations.
		/// (NOTE: Originally, this method was called: 'option_price_call_american_binomial'
		/// that name was already in use and didn't mention the 'payout' properties of 
		/// the method, so the new name is: 'option_price_call_american_binomial_payout')
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="y">continuous payout</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallAmericanBinomialPayout(double S, double K, double r,
			double y, double sigma, double t, int steps)
		{
			double R = Math.Exp(r*(t/steps));            // interest rate for each step
			double Rinv = 1.0/R;                    // inverse of interest rate
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));    // up movement
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (Math.Exp((r-y)*(t/steps))-d)/(u-d);
			double p_down = 1.0-p_up;
			double[] prices = new double[steps+1];       // price of underlying
			prices[0] = S*Math.Pow(d, steps);  
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1]; // fill in the endnodes.

			double[] call_values = new double[steps+1];       // value of corresponding call 
			for (int i=0; i<=steps; ++i) 
				call_values[i] = Math.Max(0.0, (prices[i]-K)); // call payoffs at maturity

			for (int step=steps-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i) 
				{
					call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
					prices[i] = d*prices[i+1];
					call_values[i] = Math.Max(call_values[i],prices[i]-K);       // check for exercise
				}
			}
			return call_values[0];
		}

		/// <summary>
		/// American Option (Put) with continuous payouts using binomial 
		/// approximations.
		/// (NOTE: Originally, this method was called: 'option_price_put_american_binomial'
		/// that name was already in use and didn't mention the 'payout' properties of 
		/// the method, so the new name is: 'option_price_put_american_binomial_payout')
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="y">continuous payout</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutAmericanBinomialPayout(double S, double K, double r,
			double y, double sigma, double t, int steps)
		{
			double R = Math.Exp(r*(t/steps));            // interest rate for each step
			double Rinv = 1.0/R;                    // inverse of interest rate
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));    // up movement
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (Math.Exp((r-y)*(t/steps))-d)/(u-d);
			double p_down = 1.0-p_up;
			double[] prices = new double[steps+1];       // price of underlying
			double[] put_values = new double[steps+1];       // value of corresponding put 

			prices[0] = S*Math.Pow(d, steps);  // fill in the endnodes.
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1];
			for (int i=0; i<=steps; ++i) 
				put_values[i] = Math.Max(0.0, (K-prices[i])); // put payoffs at maturity
			for (int step=steps-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i) 
				{
					put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
					prices[i] = d*prices[i+1];
					put_values[i] = Math.Max(put_values[i],(K-prices[i]));       // check for exercise
				}
			}
			return put_values[0];
		}

		/// <summary>
		/// European Option (Call) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallEuropeanBinomial(double S, double K, double r, 
			double sigma, double t, int steps)
		{
			double R = Math.Exp(r*(t/steps));            // interest rate for each step
			double Rinv = 1.0/R;                    // inverse of interest rate
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));    // up movement
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (R-d)/(u-d);
			double p_down = 1.0-p_up;
			double[] prices = new double[steps+1];       // price of underlying
			prices[0] = S*Math.Pow(d, steps);  // fill in the endnodes.
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1];
			double[] call_values = new double[steps+1];       // value of corresponding call 
			for (int i=0; i<=steps; ++i) 
				call_values[i] = Math.Max(0.0, (prices[i]-K)); // call payoffs at maturity
			for (int step=steps-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i) 
				{
					call_values[i] = (p_up*call_values[i+1]+p_down*call_values[i])*Rinv;
				}
			}
			return call_values[0];
		}

		/// <summary>
		/// European Option (Put) using binomial approximations
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="t">time to maturity</param>
		/// <param name="steps">Number of steps in binomial tree</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutEuropeanBinomial(double S, double K, double r,
			double sigma, double t, int steps)
		{
			double R = Math.Exp(r*(t/steps));            // interest rate for each step
			double Rinv = 1.0/R;                    // inverse of interest rate
			double u = Math.Exp(sigma*Math.Sqrt(t/steps));    // up movement
			double uu = u*u;
			double d = 1.0/u;
			double p_up = (R-d)/(u-d);
			double p_down = 1.0-p_up;
			double[] prices = new double[steps+1];       // price of underlying
			prices[0] = S*Math.Pow(d, steps);  // fill in the endnodes.
			for (int i=1; i<=steps; ++i) 
				prices[i] = uu*prices[i-1];
			double[] put_values = new double[steps+1];       // value of corresponding put 
			for (int i=0; i<=steps; ++i) 
				put_values[i] = Math.Max(0.0, (K-prices[i])); // put payoffs at maturity
			for (int step=steps-1; step>=0; --step) 
			{
				for (int i=0; i<=step; ++i) 
				{
					put_values[i] = (p_up*put_values[i+1]+p_down*put_values[i])*Rinv;
				}
			}
			return put_values[0];
		}
	}
}
