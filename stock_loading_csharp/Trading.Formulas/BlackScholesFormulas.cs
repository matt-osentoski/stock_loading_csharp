﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Formulas
{
	/// <summary>
	/// This class contains formulas based on the Black Scholes model
	///     
	/// Converted to C# from "Financial Numerical Recipes in C" by:
	/// Bernt Arne Odegaard
	/// http://finance.bi.no/~bernt/gcc_prog/index.html
	/// </summary>
	public class BlackScholesFormulas
	{
		/// <summary>
		/// Normal distribution
		/// </summary>
		/// <param name="z">Value to test</param>
		/// <returns>Normal distribution</returns>
		public static double NormDist(double z) 
		{     
			return (1.0/Math.Sqrt(2.0*Math.PI))*Math.Exp(-0.5*z*z);
		}

		/// <summary>
		/// Cumulative normal distribution
		/// Abramowiz and Stegun approximation (1964)
		/// </summary>
		/// <param name="z">Value to test</param>
		/// <returns>Cumulative normal distribution</returns>
		public static double CumulativeNormDist(double z)
		{
			if (z > 6.0) { return 1.0; }; // this guards against overflow 
			if (z < -6.0) { return 0.0; };

			double b1 = 0.31938153;
			double b2 = -0.356563782;
			double b3 = 1.781477937;
			double b4 = -1.821255978;
			double b5 = 1.330274429;
			double p = 0.2316419;
			double c2 = 0.3989423;

			double a = Math.Abs(z);
			double t = 1.0 / (1.0 + a * p);
			double b = c2 * Math.Exp((-z) * (z / 2.0));
			double n = ((((b5 * t + b4) * t + b3) * t + b2) * t + b1) * t;
			n = 1.0 - b * n;
			if (z < 0.0) 
				n = 1.0 - n;
			return n; 
		}

		/// <summary>
		/// Black Scholes formula (Call)
		/// Black and Scholes (1973) and Merton (1973)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <returns>Option price</returns>
		public static double OptionPriceCallBlackScholes(double S, double K, double r, double sigma, double time)
		{
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S/K)+r*time)/(sigma*time_sqrt)+0.5*sigma*time_sqrt; 
			double d2 = d1-(sigma*time_sqrt);
			return S * CumulativeNormDist(d1) - K * Math.Exp(-r * time) * CumulativeNormDist(d2);
		}

		/// <summary>
		/// Black Scholes formula (Put)
		/// Black and Scholes (1973) and Merton (1973)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <returns>Option price</returns>
		public static double OptionPricePutBlackScholes(double S, double K, double r, double sigma, double time)
		{
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt;
			double d2 = d1-(sigma*time_sqrt);
			return K * Math.Exp(-r * time) * CumulativeNormDist(-d2) - S * CumulativeNormDist(-d1);
		}

		/// <summary>
		/// Delta of the Black Scholes formula (Call)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <returns>Delta of the option</returns>
		public static double OptionPriceDeltaCallBlackScholes(double S, double K, double r, double sigma, double time)
		{
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S / K) + r * time) / (sigma * time_sqrt) + 0.5 * sigma * time_sqrt;
			double delta = CumulativeNormDist(d1);
			return delta;
		}

		/// <summary>
		/// Delta of the Black Scholes formula (Put)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <returns>Delta of the option</returns>
		public static double OptionPriceDeltaPutBlackScholes(double S, double K, double r, double sigma, double time)
		{
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S / K) + r * time) / (sigma * time_sqrt) + 0.5 * sigma * time_sqrt;
			double delta = -CumulativeNormDist(-d1);
			return delta;
		}

		/// <summary>
		/// Calculates implied volatility for the Black Scholes formula using
		/// binomial search algorithm
		/// (NOTE: In the original code a large negative number was used as an
		/// exception handling mechanism.  This has been replace with a generic
		/// 'Exception' that is thrown.  The original code is in place and commented
		/// if you want to use the pure version of this code)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="time">time to maturity</param>
		/// <param name="optionPrice">The price of the option</param>
		/// <returns>Sigma (implied volatility)</returns>
		public static double OptionPriceImpliedVolatilityCallBlackScholesBisections(double S, double K, double r,
			double time, double optionPrice)
		{
			// check for arbitrage violations.
			if (optionPrice < 0.99 * (S - K * Math.Exp(-time * r)))
			{   
				return 0.0;                             // Option price is too low if this happens
			}

			// simple binomial search for the implied volatility.
			// relies on the value of the option increasing in volatility
			double ACCURACY = 1.0e-5; // make this smaller for higher accuracy
			int MAX_ITERATIONS = 100;
			double HIGH_VALUE = 1e10;
			//const double ERROR = -1e40;  // <--- original code

			// want to bracket sigma. first find a maximum sigma by finding a sigma
			// with a estimated price higher than the actual price.
			double sigma_low = 1e-5;
			double sigma_high = 0.3;
			double price = OptionPriceCallBlackScholes(S, K, r, sigma_high, time);
			while (price < optionPrice)
			{
				sigma_high = 2.0 * sigma_high; // keep doubling.
				price = OptionPriceCallBlackScholes(S, K, r, sigma_high, time);
				if (sigma_high > HIGH_VALUE)
				{
					//return ERROR; // panic, something wrong.     // <--- original code
					throw new Exception("panic, something wrong."); // Comment this line if you uncomment the line above
				}
			}
			for (int i = 0; i < MAX_ITERATIONS; i++)
			{
				double sigma = (sigma_low + sigma_high) * 0.5;
				price = OptionPriceCallBlackScholes(S, K, r, sigma, time);
				double test = (price - optionPrice);
				if (Math.Abs(test) < ACCURACY) 
					return sigma; 
				if (test < 0.0) 
					sigma_low = sigma; 
				else
					sigma_high = sigma; 
			}
			//return ERROR;      // <--- original code
			throw new Exception("An error occurred"); // Comment this line if you uncomment the line above
		}

		/// <summary>
		/// Calculates implied volatility for the Black Scholes formula using
		/// the Newton-Raphson formula
		/// (NOTE: In the original code a large negative number was used as an
		/// exception handling mechanism.  This has been replace with a generic
		/// 'Exception' that is thrown.  The original code is in place and commented
		/// if you want to use the pure version of this code)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="time">time to maturity</param>
		/// <param name="optionPrice">The price of the option</param>
		/// <returns>Sigma (implied volatility)</returns>
		public static double OptionPriceImpliedVolatilityCallBlackScholesNewton(double S, double K, double r,
			double time, double optionPrice)
		{
			// check for arbitrage violations. Option price is too low if this happens
			if (optionPrice < 0.99 * (S - K * Math.Exp(-time * r)))
			{  
				return 0.0;
			}

			int MAX_ITERATIONS = 100;
			double ACCURACY = 1.0e-5;
			double t_sqrt = Math.Sqrt(time);

			double sigma = (optionPrice / S) / (0.398 * t_sqrt);    // find initial value
			for (int i = 0; i < MAX_ITERATIONS; i++)
			{
				double price = OptionPriceCallBlackScholes(S, K, r, sigma, time);
				double diff = optionPrice - price;
				if (Math.Abs(diff) < ACCURACY) 
					return sigma;
				double d1 = (Math.Log(S / K) + r * time) / (sigma * t_sqrt) + 0.5 * sigma * t_sqrt;
				double vega = S * t_sqrt * NormDist(d1);
				sigma = sigma + diff / vega;
			}
			//return -99e10;  // something screwy happened, should throw exception  // <--- original code
			throw new Exception("An error occurred"); // Comment this line if you uncomment the line above
		}

		/// <summary>
		/// Calculate partial derivatives for a Black Scholes Option (Call)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="delta">Output - partial derivatives with respect to (wrt) S</param>
		/// <param name="gamma">Output - second partial derivatives wrt S</param>
		/// <param name="theta">Output - partial derivatives wrt time</param>
		/// <param name="vega">Output - partial derivatives wrt sigma</param>
		/// <param name="rho">Output - partial derivatives wrt r</param>
		public static void OptionPricePartialsCallBlackScholes( double S, double K, double r, double sigma,
			double time, out double delta, out double gamma, out double theta, out double vega, out double rho)
		{ 
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S/K)+r*time)/(sigma*time_sqrt) + 0.5*sigma*time_sqrt; 
			double d2 = d1-(sigma*time_sqrt);
			delta = CumulativeNormDist(d1);
			gamma = NormDist(d1) / (S * sigma * time_sqrt);
			theta = -(S * sigma * NormDist(d1)) / (2 * time_sqrt) - r * K * Math.Exp(-r * time) * CumulativeNormDist(d2);
			vega = S * time_sqrt * NormDist(d1);
			rho = K * time * Math.Exp(-r * time) * CumulativeNormDist(d2);
		}

		/// <summary>
		/// Calculate partial derivatives for a Black Scholes Option (Put)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <param name="delta">Output - partial derivatives with respect to (wrt) S</param>
		/// <param name="gamma">Output - second partial derivatives wrt S</param>
		/// <param name="theta">Output - partial derivatives wrt time</param>
		/// <param name="vega">Output - partial derivatives wrt sigma</param>
		/// <param name="rho">Output - partial derivatives wrt r</param>
		public static void OptionPricePartialsPutBlackScholes(double S, double K, double r, double sigma,
			double time, out double delta, out double gamma, out double theta, out double vega, out double rho)
		{
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S / K) + r * time) / (sigma * time_sqrt) + 0.5 * sigma * time_sqrt;
			double d2 = d1 - (sigma * time_sqrt);
			delta = -CumulativeNormDist(-d1);
			gamma = NormDist(d1) / (S * sigma * time_sqrt);
			theta = -(S * sigma * NormDist(d1)) / (2 * time_sqrt) + r * K * Math.Exp(-r * time) * CumulativeNormDist(-d2);
			vega = S * time_sqrt * NormDist(d1);
			rho = -K * time * Math.Exp(-r * time) * CumulativeNormDist(-d2);
		}

		/// <summary>
		/// European option (Call) with a continuous payout. 
		/// The continuous payout would be for fees associated with the asset.
		/// For example, storage costs.
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="X">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="q">yield on underlying</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <returns>Option price</returns>
		public static double OptionPriceEuropeanCallPayout(double S, double X, double r, double q, 
			double sigma, double time)
		{
			double sigma_sqr = Math.Pow(sigma, 2);
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S / X) + (r - q + 0.5 * sigma_sqr) * time) / (sigma * time_sqrt);
			double d2 = d1 - (sigma * time_sqrt);
			double call_price = S * Math.Exp(-q * time) * CumulativeNormDist(d1) - X * Math.Exp(-r * time) * CumulativeNormDist(d2);
			return call_price;
		}

		/// <summary>
		/// European option (Put) with a continuous payout. 
		/// The continuous payout would be for fees associated with the asset.
		/// For example, storage costs.
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="q">yield on underlying</param>
		/// <param name="sigma">volatility</param>
		/// <param name="time">time to maturity</param>
		/// <returns>Option price</returns>
		public static double OptionPriceEuropeanPutPayout(double S, double K, double r, double q,
			double sigma, double time)
		{
			double sigma_sqr = Math.Pow(sigma, 2);
			double time_sqrt = Math.Sqrt(time);
			double d1 = (Math.Log(S / K) + (r - q + 0.5 * sigma_sqr) * time) / (sigma * time_sqrt);
			double d2 = d1 - (sigma * time_sqrt);
			double put_price = K * Math.Exp(-r * time) * CumulativeNormDist(-d2) - S * Math.Exp(-q * time) * CumulativeNormDist(-d1);
			return put_price;
		}

		/// <summary>
		/// European option for known dividends (Call)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="timeToMaturity">time to maturity</param>
		/// <param name="dividendTimes">Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="dividendAmounts">Array of dividend amounts for the 'dividendTimes'</param>
		/// <returns>Option price</returns>
		public static double OptionPriceEuropeanCallDividends(double S, double K, double r, 
			double sigma, double timeToMaturity, double[] dividendTimes, double[] dividendAmounts)
		{
			double adjusted_S = S;
			for (int i = 0; i < dividendTimes.Count(); i++)
			{
				if (dividendTimes[i] <= timeToMaturity)
				{
					adjusted_S -= dividendAmounts[i] * Math.Exp(-r * dividendTimes[i]);
				}
			}
			return OptionPriceCallBlackScholes(adjusted_S, K, r, sigma, timeToMaturity);
		}

		/// <summary>
		/// European option for known dividends (Put)
		/// </summary>
		/// <param name="S">spot (underlying) price</param>
		/// <param name="K">strike (exercise) price</param>
		/// <param name="r">interest rate</param>
		/// <param name="sigma">volatility</param>
		/// <param name="timeToMaturity">time to maturity</param>
		/// <param name="dividendTimes">Array of dividend times. (Ex: [0.25, 0.75] for 1/4 and 3/4 of a year)</param>
		/// <param name="dividendAmounts">Array of dividend amounts for the 'dividendTimes'</param>
		/// <returns>Option price</returns>
		public static double OptionPriceEuropeanPutDividends(double S, double K, double r,
			double sigma, double timeToMaturity, double[] dividendTimes, double[] dividendAmounts)
		{
			double adjusted_S = S;
			for (int i = 0; i < dividendTimes.Count(); i++) 
			{
				if (dividendTimes[i]<=timeToMaturity)
				{
					adjusted_S -= dividendAmounts[i] * Math.Exp(-r*dividendTimes[i]);
				}
			}
			return OptionPricePutBlackScholes(adjusted_S,K,r,sigma,timeToMaturity);
		}
	}
}
