﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Strategies
{
    public interface IStrategy
    {
        void Process();
    }
}
