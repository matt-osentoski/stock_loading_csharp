﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trading.Data;
using MathNet.Numerics.Distributions;

namespace Trading.Strategies.Utils
{
    public class RandomPriceGenerator
    {
        private double StartPriceLow;
        private double StartPriceHigh;
        private double TickMove;
        private Random rand;
        private int tickMultiplier;

        public RandomPriceGenerator(double startPriceLow = 1, double startPriceHigh = 150, double tickMove = 0.01, int tickMultiplier = 75)
        {
            this.StartPriceLow = startPriceLow;
            this.StartPriceHigh = startPriceHigh;
            this.TickMove = tickMove;
            this.tickMultiplier = tickMultiplier;
            this.rand = new Random();
        }

        /// <summary>
        /// Creates a list of stock prices based on a simple random generator that moves up or down
        /// one tick per move.
        /// </summary>
        /// <param name="size">The number of StockPrice objects to generate</param>
        /// <returns>A list of stock prices</returns>
        public List<StockPrice> BasicUpDownGenerator(int size = 3000)
        {
            List<StockPrice> prices = new List<StockPrice>();
            StockPrice lastPrice = null;
            for (int x = 0; x < size; x++)
            {
                var price = this.UpDownDaily(lastPrice, 3, true);
                lastPrice = price;
                prices.Add(price);
            }
            return prices;
        }

        /// <summary>
        /// Creates one StockPrice value with a open, close, up, and down value.  
        /// This is done by determining a number of ticks per day moving up/down 
        /// randomly per tick.
        /// </summary>
        /// <param name="lastPrice">The last StockPrice object used to create a continuation of stock prices</param>
        /// <param name="gap">The number of ticks to use for a gap.  This will be random, so if you choose 20, it will be a number between 0 - 20</param>
        /// <returns>Returns a random stock price with a value related to the last stock price</returns>
        private StockPrice UpDownDaily(StockPrice lastPrice, int gap = 0, bool blackSwan = false)
        {
            if (lastPrice == null)
            {
                lastPrice = new StockPrice();
                // Generate a starting stock value
                var randomPrice = this.rand.NextDouble() * (this.StartPriceHigh - this.StartPriceLow) + this.StartPriceLow;
                var startPrice = Math.Round(randomPrice, 2);
                lastPrice.closing_price = new Decimal(startPrice);

                // Generate a starting stock date
                // NOTE: Since these prices are random, it doesn't matter if every
                // series of prices use the same start date.
                // In the future, I might revisit this and make the start dates random as well.
                DateTime startDate = new DateTime(1981, 1, 1);
                lastPrice.price_date = startDate;
            }

            var price = (double)lastPrice.closing_price;
            var stockPrice = new StockPrice();

            double lowPrice = 0;
            double highPrice = 0;

            // Determine the number of ticks to use for this day's price movement
            // After some testing, I have the multiplier constant.  I'll probably tweak this over time.
            // NOTE: Since the starting price is determined by the gap, the daily ticks should never be less than
            // the gap value plus 1 for padding. 
            int dailyTicksMax = (int)price * this.tickMultiplier;
            if (dailyTicksMax < gap)
                dailyTicksMax = gap + 1;
            
            var dailyTicks = this.rand.Next(gap + 1, dailyTicksMax + 1);

            // Even with all the random variations, the prices produced still aren't very realistic.  Sometimes (rarely) a
            // major move occurs.  This seems to happen in the market every 7-10 years.  
            // To simulate these events, change the number of ticks to 1,000,000 / day if this rare event occurs.
            // This should have the effect of drastically altering the volatility.
            // For now, I'll default the probability as 1 in 1764. (252 trading days per year * 7 years = 1764)
            int godsWill = this.rand.Next(1764);
            if (blackSwan && godsWill == 3)  // I'm just arbitralily using 3 here
                dailyTicks = 1000000;

            // First price using gapping value
            var startPriceIdx = this.rand.Next(gap + 1);

            for (int x = 0; x < dailyTicks; x++)
            {
                if (x == startPriceIdx)
                {
                    stockPrice.opening_price = new Decimal(Math.Round(price, 2));
                    lowPrice = price;
                    highPrice = price;
                }

                
                // Use a random 0/1 to determine if the tick should go up or down.
                var upDown = this.rand.Next(1 + 1);
                if (upDown == 1)
                    price += this.TickMove;
                else
                    price -= this.TickMove;

                if (price <= 0)
                    break;

                // Set the high/low prices, as needed
                if (price < lowPrice)
                    lowPrice = price;
                if (price > highPrice)
                    highPrice = price;
            }
            if (price <= 0)
                stockPrice.closing_price = 0;
            else
                stockPrice.closing_price = new Decimal(Math.Round(price, 2));
            stockPrice.low_price = new Decimal(Math.Round(lowPrice, 2));
            stockPrice.high_price = new Decimal(Math.Round(highPrice, 2));
            stockPrice.price_date = ((DateTime)lastPrice.price_date).AddDays(1);

            return stockPrice;
        }
    }
}
