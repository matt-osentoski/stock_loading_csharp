﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trading.Strategies.Utils;
using Trading.Strategies.Model;

namespace Trading.Strategies.Stocks
{
    public class StoppedOutRandomStrategy : IStrategy
    {
        private static double StopLossPercentage = 0.035;
        private static double RaiseStopLossPercentage = 0.005;
        private static double SellEquityPercentage = 0.020;
        private static double SaleStopEquityPercentage = 0.005;
        private static double TradingAccount = 10000;
        private static double CommisionFee = 7;
        private RandomPriceGenerator priceGen;
        private int stopCount = 0;

        public void Process()
        {
            // IMPORTANT!!!! Make sure the RandomPriceGenerator is instantiated only once
            // otherwise, the values won't be as random as they should be.
            priceGen = new RandomPriceGenerator(5, 25); // Stocks ranging from $1 - $25 / share
            var account = new TradingAccount();
            account.StartingAccountEquity = TradingAccount;
            account.AccountEquity = TradingAccount;
            int win = 0;
            int loss = 0;
            for (int x = 0; x < 20; x++)
            {
                var startTotal = account.AccountEquity;
                this.TradeStock(account);
                if (account.AccountEquity > startTotal)
                    win++;
                else
                    loss++;
            }
            Console.WriteLine("Account total after trading: " + account.AccountEquity);
            Console.WriteLine("Wins: " + win + "   Losses: " + loss);
        }

        public void TradeStock(TradingAccount account)
        {
            var prices = priceGen.BasicUpDownGenerator(2000);

            var accountBalance = account.AccountEquity;

            var tradingSummary = new TradingSummary();
            tradingSummary.PurchasePrice = (double)prices.ElementAt(0).closing_price;

            tradingSummary.CommisionCost = CommisionFee;
            int x = 0;
            double currentPrice = tradingSummary.PurchasePrice;

            // Determine the number of shares to buy based on the current price and the balance in your trading account.
            int sharesActual = (int)((accountBalance - (tradingSummary.CommisionCost * 2)) / currentPrice);
            // Shares should be a multiple of 100, rounded down
            if (sharesActual < 100)
                return;
            int shares = ((int)Math.Floor( (double)(sharesActual/ 100) )) * 100;
            
            tradingSummary.NumberOfShares = shares;
            double tradeStartingEquity = currentPrice * shares;
            double tradeEquity = tradeStartingEquity;

            double sellPrice = currentPrice;
            double stopLossPrice = tradeStartingEquity - (tradeStartingEquity * StopLossPercentage);

            foreach (var price in prices)
            {
                if (x == 0)  // skip the first price
                {
                    x++;
                    continue;
                }

                if ((double)price.opening_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.opening_price;
                    break;
                }  
                // The low price can't be accurately used, because most likely the stop will have kicked in before the low 
                // is reached.  To get around this problem I'm taking a random loss between the stop and the low.
                // Note that the 'open price' is used above since a gap between days cannot be avoided.
                if ((double)price.low_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.low_price;
                    var tmpLowTotal = sellPrice * shares;

                    var lowPercent = 1 - (tmpLowTotal / stopLossPrice);
                    if (lowPercent > StopLossPercentage)
                    {
                        var randomPercent = new Random().NextDouble() * (lowPercent - StopLossPercentage) + StopLossPercentage;
                        sellPrice = (tradeEquity - (tradeEquity * randomPercent)) / shares;
                    }
                    break;
                }                  
                if ((double)price.high_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.high_price;
                    break;
                }
                if ((double)price.closing_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.closing_price;
                    break;
                }

                var basePrice = (stopLossPrice + (stopLossPrice * StopLossPercentage));
                var expectedSalePrice = tradeStartingEquity + (tradeStartingEquity * SellEquityPercentage);
                bool isExpectedSale = false;
                if ((double)price.closing_price * shares >= expectedSalePrice && !isExpectedSale)
                {
                    var tmpEquity = (double)price.closing_price * shares;
                    stopLossPrice = tmpEquity - (tmpEquity * SaleStopEquityPercentage);
                    isExpectedSale = true;
                }
                if ((double)price.high_price * shares >= expectedSalePrice && !isExpectedSale)
                {
                    var tmpEquity = (double)price.high_price * shares;
                    stopLossPrice = tmpEquity - (tmpEquity * SaleStopEquityPercentage);
                    isExpectedSale = true;
                }       
                if ((double)price.opening_price * shares >= expectedSalePrice && !isExpectedSale)
                {
                    var tmpEquity = (double)price.opening_price * shares;
                    stopLossPrice = tmpEquity - (tmpEquity * SaleStopEquityPercentage);
                    isExpectedSale = true;
                }
                if ((double)price.low_price * shares >= expectedSalePrice && !isExpectedSale)
                {
                    var tmpEquity = (double)price.low_price * shares;
                    stopLossPrice = tmpEquity - (tmpEquity * SaleStopEquityPercentage);
                    isExpectedSale = true;
                }
                if (isExpectedSale)
                    Console.WriteLine("Stoploss* updated: " + stopLossPrice);


                var newStopLossPrice = this.ChangeStopPrice((double)price.closing_price, shares, stopLossPrice, RaiseStopLossPercentage);
                if (newStopLossPrice != null)
                {
                    stopLossPrice = (double)newStopLossPrice;
                    Console.WriteLine("Stoploss updated: " + stopLossPrice);
                }
                tradeEquity = sellPrice * shares;    
                x++;
            }
            tradeEquity = sellPrice * shares;
            tradingSummary.DaysTraded = x + 1;
            tradingSummary.SellingPrice = sellPrice;
            double pAndL = tradeEquity - tradeStartingEquity;
            Console.WriteLine("Shares: " + shares);
            Console.WriteLine("Trading Days: " + tradingSummary.DaysTraded);
            Console.WriteLine("Purchase/Sale Price:   " + tradingSummary.PurchasePrice + " / " + tradingSummary.SellingPrice);
            Console.WriteLine("Starting/Ending Trade Equity:   " + tradeStartingEquity + " / " + tradeEquity);
            Console.WriteLine("P/L: " + pAndL);

            account.AccountEquity = (account.AccountEquity + pAndL) - (tradingSummary.CommisionCost * 2);
        }

        private double? ChangeStopPrice(double closingPrice, int shares, double stopLossPrice, double raisePercent = 0.03)
        {
            var tradeEquity = closingPrice * shares;
            var priceDiff = tradeEquity - stopLossPrice;
            if (priceDiff <= 0)
                return null;
            var basePrice = (stopLossPrice + (stopLossPrice * StopLossPercentage));
            var nextRaisePrice = basePrice + (basePrice * raisePercent);
            if (tradeEquity > nextRaisePrice)
            {
                return tradeEquity - (tradeEquity * (StopLossPercentage - 0.005));
            }
            return null;
        }
    }
}
