﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trading.Strategies.Utils;
using Trading.Strategies.Model;

namespace Trading.Strategies.Stocks
{
    public class MovingAvgRandomStrategy : IStrategy
    {
        private static double StopLossPercentage = 0.02;
        private static double RaiseStopLossPercentage = 0.010;
        private static double SellEquityPercentage = 0.025;
        private static double TradingAccount = 10000;
        private static double CommisionFee = 7;
        private RandomPriceGenerator priceGen;
        private int skipped = 0;

        public void Process()
        {
            // IMPORTANT!!!! Make sure the RandomPriceGenerator is instantiated only once
            // otherwise, the values won't be as random as they should be.
            priceGen = new RandomPriceGenerator(5, 25); // Stocks ranging from $1 - $25 / share
            var account = new TradingAccount();
            account.StartingAccountEquity = TradingAccount;
            account.AccountEquity = TradingAccount;
            int win = 0;
            int loss = 0;
            for (int x = 0; x < 100; x++)
            {
                var startTotal = account.AccountEquity;
                this.TradeStock(account);
                if (account.AccountEquity > startTotal)
                    win++;
                else
                    loss++;
            }
            Console.WriteLine("Account total after trading: " + account.AccountEquity);
            Console.WriteLine("Wins: " + win + "   Losses: " + (loss - skipped));
            Console.WriteLine("Skipped: " + skipped);
        }

        public void TradeStock(TradingAccount account)
        {
            var prices = priceGen.BasicUpDownGenerator(2000);

            var accountBalance = account.AccountEquity;

            var tradingSummary = new TradingSummary();
            tradingSummary.PurchasePrice = (double)prices.ElementAt(0).closing_price;

            tradingSummary.CommisionCost = CommisionFee;
            int x = 0;
            double currentPrice = tradingSummary.PurchasePrice;

            // Determine the number of shares to buy based on the current price and the balance in your trading account.
            int sharesActual = (int)((accountBalance - (tradingSummary.CommisionCost * 2)) / currentPrice);
            // Shares should be a multiple of 100, rounded down
            if (sharesActual < 100)
                return;
            int shares = ((int)Math.Floor( (double)(sharesActual/ 100) )) * 100;
            tradingSummary.NumberOfShares = shares;

            double tradeStartingEquity = currentPrice * shares;
            double tradeEquity = tradeStartingEquity;

            double sellPrice = currentPrice;
            double stopLossPrice = tradeStartingEquity - (tradeStartingEquity * StopLossPercentage);

            // Skip stocks that aren't rising
            double lastPrice = 0;
            int skipIdx = 1;
            for (int y = 0; y < prices.Count; y++)
            {
                if ((double)prices.ElementAt(y).closing_price > lastPrice)
                {
                    lastPrice = (double)prices.ElementAt(y).closing_price;
                    if (y > skipIdx)
                        break;
                }
                else
                {
                    skipped++;
                    Console.WriteLine("Falling Price, skipped");
                    return;
                }
            }

            foreach (var price in prices)
            {
                if (x < skipIdx)  // skip the first price
                {
                    x++;
                    continue;
                }
                else if (x == skipIdx + 1)
                {
                    tradingSummary.PurchasePrice = (double)price.closing_price;
                    currentPrice = tradingSummary.PurchasePrice;
                    tradeStartingEquity = currentPrice * shares;
                    tradeEquity = tradeStartingEquity;
                    sellPrice = currentPrice;
                    stopLossPrice = tradeStartingEquity - (tradeStartingEquity * StopLossPercentage);
                }

                if ((double)price.opening_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.opening_price;
                    break;
                }
                // The low price can't be accurately used, because most likely the stop will have kicked in before the low 
                // is reached.  To get around this problem I'm taking a random loss between the stop and the low.
                // Note that the 'open price' is used above since a gap between days cannot be avoided.
                if ((double)price.low_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.low_price;
                    var tmpLowTotal = sellPrice * shares;

                    var lowPercent = 1 - (tmpLowTotal / stopLossPrice);
                    if (lowPercent > StopLossPercentage)
                    {
                        var randomPercent = new Random().NextDouble() * (lowPercent - StopLossPercentage) + StopLossPercentage;
                        sellPrice = (tradeEquity - (tradeEquity * randomPercent)) / shares;
                    }
                    break;
                }
                if ((double)price.high_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.high_price;
                    break;
                }
                if ((double)price.closing_price * shares < stopLossPrice)
                {
                    sellPrice = (double)price.closing_price;
                    break;
                }

                var basePrice = (stopLossPrice + (stopLossPrice * StopLossPercentage));
                var expectedSalePrice = tradeStartingEquity + (tradeStartingEquity * SellEquityPercentage);
                if ((double)price.low_price * shares >= expectedSalePrice)
                {
                    sellPrice = (double)price.low_price;
                    break;
                }
                if ((double)price.opening_price * shares >= expectedSalePrice)
                {
                    sellPrice = (double)price.opening_price;
                    break;
                }
                if ((double)price.closing_price * shares >= expectedSalePrice)
                {
                    sellPrice = (double)price.closing_price;
                    break;
                }
                if ((double)price.high_price * shares >= expectedSalePrice)
                {
                    sellPrice = (double)price.high_price;
                    break;
                }
                var newStopLossPrice = this.ChangeStopPrice((double)price.closing_price, shares, stopLossPrice, RaiseStopLossPercentage);
                if (newStopLossPrice != null)
                {
                    stopLossPrice = (double)newStopLossPrice;
                    Console.WriteLine("Stoploss updated");
                }
                tradeEquity = sellPrice * shares;
                x++;
            }
            tradeEquity = sellPrice * shares;
            tradingSummary.DaysTraded = x + 1;
            tradingSummary.SellingPrice = sellPrice;
            double pAndL = tradeEquity - tradeStartingEquity;
            Console.WriteLine("Shares: " + shares);
            Console.WriteLine("Trading Days: " + tradingSummary.DaysTraded);
            Console.WriteLine("Purchase Price: " + tradingSummary.PurchasePrice);
            Console.WriteLine("Sale Price: " + tradingSummary.SellingPrice);
            Console.WriteLine("Starting Trade Equity: " + tradeStartingEquity);
            Console.WriteLine("Ending Trade Equity: " + tradeEquity);
            Console.WriteLine("P/L: " + pAndL);

            account.AccountEquity = (account.AccountEquity + pAndL) - (tradingSummary.CommisionCost * 2);
        }

        private double? ChangeStopPrice(double closingPrice, int shares, double stopLossPrice, double raisePercent = 0.03)
        {
            var tradeEquity = closingPrice * shares;
            var priceDiff = tradeEquity - stopLossPrice;
            if (priceDiff <= 0)
                return null;
            var basePrice = (stopLossPrice + (stopLossPrice * StopLossPercentage));
            var nextRaisePrice = basePrice + (basePrice * raisePercent);
            if (tradeEquity > nextRaisePrice)
            {
                return tradeEquity - (tradeEquity * (StopLossPercentage - 0.005));
            }
            return null;
        }
    }
}
