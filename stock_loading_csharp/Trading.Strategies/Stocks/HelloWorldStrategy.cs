﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Strategies.Stocks
{
    public class HelloWorldStrategy : IStrategy
    {
        public void Process()
        {
            Console.WriteLine("Hello World!");
        }
    }
}
