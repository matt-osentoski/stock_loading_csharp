﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Strategies.Model
{
    public class TradingSummary
    {
        public string Symbol { get; set; }
        public double ExpectedPurchasePrice { get; set; }
        public double PurchasePrice { get; set; }
        public int NumberOfShares { get; set; }
        public double SellingPrice { get; set; }
        public double ExpectedSellingPrice { get; set; }
        public double CommisionCost { get; set; }
        public int DaysTraded { get; set; }
        public DateTime PurchaseDate { get; set; }
        public DateTime SellingDate { get; set; }

        private double profitLoss;

        public double ProfitLoss
        {
            get 
            {
                return this.SellingPrice - this.PurchasePrice;
            }
        }
        
    }
}
