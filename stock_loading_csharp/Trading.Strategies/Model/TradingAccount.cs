﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Strategies.Model
{
    public class TradingAccount
    {
        public double StartingAccountEquity { get; set; }
        public double AccountEquity { get; set; }
    }
}
