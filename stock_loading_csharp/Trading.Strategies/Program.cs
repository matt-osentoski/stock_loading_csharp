﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using System.Configuration;
using NDesk.Options;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Trading.Strategies
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize the logger.
            var log = LogManager.GetLogger(typeof(Program));

            // Grab the default strategy implementation from the config file
            string strategyImplementation = ConfigurationManager.AppSettings["ConcreteStrategy"];

            // If a command line argument for a strategy was used, implement that instead.
            OptionSet options = new OptionSet() 
            { 
                {"s=|strategy=", s => {strategyImplementation = s;}},
                {"?|h|help", h => {DisplayHelp();}}
            };
            options.Parse(args);

            UnityContainer container = new UnityContainer();
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Configure(container);
            var strategy = container.Resolve<IStrategy>(strategyImplementation);
            strategy.Process();

        }

        static void DisplayHelp()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("-?|-h|-help : This help message");
            Console.WriteLine("-s|-strategy : Concrete implemention of the Strategy you want to run.");
            Environment.Exit(Environment.ExitCode);
        }
    }
}
