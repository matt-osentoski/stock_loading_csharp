//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Trading.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Stock
    {
        public Stock()
        {
            this.StockPrices = new HashSet<StockPrice>();
        }
    
        public int id { get; set; }
        public string symbol { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string sector { get; set; }
        public string industry { get; set; }
        public string stock_index { get; set; }
        public string is_sp500 { get; set; }
        public string is_sp1500 { get; set; }
        public string is_dow_ind { get; set; }
        public string state { get; set; }
        public string active { get; set; }
    
        public virtual ICollection<StockPrice> StockPrices { get; set; }
    }
}
