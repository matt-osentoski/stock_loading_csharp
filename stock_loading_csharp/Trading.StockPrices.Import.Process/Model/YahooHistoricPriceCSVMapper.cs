﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace Trading.StockPrices.Import.Process.Model
{
    [DelimitedRecord(",")]
    public class YahooHistoricPriceCSVMapper
    {
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")] 
        public DateTime PriceDate;
        public decimal OpenPrice;
        public decimal HighPrice;
        public decimal LowPrice;
        public decimal ClosePrice;
        public int Volume;
        public decimal AdjClosingPrice;
    }
}
