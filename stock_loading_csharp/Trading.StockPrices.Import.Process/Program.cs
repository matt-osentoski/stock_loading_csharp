﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trading.StockPrices.Import.Process.Loaders;
using Common.Logging;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System.Threading.Tasks;
using System.Threading;


namespace Trading.StockPrices.Import.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize the logger.
            var log = LogManager.GetLogger(typeof(Program));

            //Use unity here to instantiate the object
            var concreteLoader = ConfigurationManager.AppSettings["ConcreteStockPriceLoaderName"];
            UnityContainer container = new UnityContainer();
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Configure(container);         

            //NOTE: Instead of using unity, a simple direct concrete implementation could be used.  I'm
            //using Unity mostly to try it out and to make this process flexible with different implmentations
            //Simple instantiation Example:
            //var loader = new YahooHistoricPriceLoader();

            // Use multi-threading to offset the loading.  This operation is DB/Write heavy. 
            var numThreads = 1;
            var threadCount = ConfigurationManager.AppSettings["ThreadCount"];
            int.TryParse(threadCount, out numThreads);
            Task[] taskArray = new Task[numThreads];
            for (var x = 0; x < taskArray.Length; x++ )
            {
                taskArray[x] = new Task((obj) =>
                {
                    var loader = container.Resolve<IStockPriceLoader>(concreteLoader);
                    log.Info("Starting thread number: " + Thread.CurrentThread.ManagedThreadId);
                    loader.Process(); // Process the stock prices.
                }, TaskCreationOptions.LongRunning);
                taskArray[x].Start();
            }

            //IMPORTANT, you MUST wait for the tasks to finish or there's no guarantee that they will
            // Finish before the app closes
            Task.WaitAll(taskArray);
        }
    }
}
