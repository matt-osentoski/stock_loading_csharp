﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using Trading.Data;
using System.Configuration;
using System.Net;
using FileHelpers;
using Trading.StockPrices.Import.Process.Model;
using System.IO;
using System.Threading;

namespace Trading.StockPrices.Import.Process.Loaders
{
    public class YahooHistoricPriceLoader : IStockPriceLoader
    {
        protected ILog log;

        private static string YahooURL = "http://ichart.finance.yahoo.com";
        private static string[] Periods = new String[] { "d", "w", "m" };
        private static List<string> IgnoreSymbols = new List<string>();

        public YahooHistoricPriceLoader()
        {
            log = LogManager.GetLogger(this.GetType());
        }

        public void Process()
        {

            try
            {
                log.Info("Starting stock price import process using CSV files from yahoo.com");
                var configStartDateStr = ConfigurationManager.AppSettings["StartDate"];
                var endDateStr = ConfigurationManager.AppSettings["EndDate"];
                var updatedStartDateStr = ConfigurationManager.AppSettings["UpdatedStartDate"];
                var endDate = DateTime.ParseExact(endDateStr, "yyyy-MM-dd", null);

                string stockSymbol = null;

                while ((stockSymbol = StockStateManager.GetNextStock()) != null)
                {
                    using (var db = new TradingEntities())
                    {
                        var stock = db.Stocks.FirstOrDefault(s => s.symbol == stockSymbol);
                        var startDateTmp = configStartDateStr;
                        if (stock.state == "a")
                            startDateTmp = updatedStartDateStr;
                        var startDate = DateTime.ParseExact(startDateTmp, "yyyy-MM-dd", null);

                        log.Info("Processing: " + stock.symbol + " using the following thread: " + Thread.CurrentThread.ManagedThreadId);
                        foreach (var period in Periods)
                        {
                            // For some reason, Yahoo has the month values set to (month -1)
                            var url = YahooURL + "/table.csv?s=" + stock.symbol +
                                "&a=" + (startDate.Month - 1) + "&b=" + startDate.Day + "&c=" + startDate.Year +
                                "&d=" + (endDate.Month - 1) + "&e=" + endDate.Day + "&f=" + endDate.Year +
                                "&g=" + period + "&ignore=.csv";

                            using (var client = new WebClient())
                            {
                                byte[] pricesStream = null;
                                try
                                {
                                    pricesStream = client.DownloadData(url);

                                }
                                catch (Exception e)
                                {
                                    // Some stocks just have bad URLs or Yahoo data. Do NOT rethrow, allow the process to continue.
                                    log.Error("A problem occurred downloading the CSV file:" + e.Message + "\n" + e.StackTrace);
                                    continue;
                                }
                                string csvText = Encoding.ASCII.GetString(pricesStream);


                                DelimitedFileEngine engine = new DelimitedFileEngine(typeof(YahooHistoricPriceCSVMapper));
                                engine.Options.IgnoreFirstLines = 1;
                                YahooHistoricPriceCSVMapper[] stockPrices = engine.ReadString(csvText) as YahooHistoricPriceCSVMapper[];
                                var x = 0;
                                foreach (var price in stockPrices)
                                {
                                    var stockPrice = new StockPrice()
                                    {
                                        symbol = stock.symbol,
                                        price_date = price.PriceDate,
                                        opening_price = price.OpenPrice,
                                        high_price = price.HighPrice,
                                        low_price = price.LowPrice,
                                        closing_price = price.ClosePrice,
                                        volume = price.Volume,
                                        adj_closing_price = price.AdjClosingPrice,
                                        period = period
                                    };
                                    stock.StockPrices.Add(stockPrice);

                                    // To keep the process from running out of memory, save changes for every 500 rows 
                                    if (x % 500 == 0 && x != 0)
                                        db.SaveChanges();  // By waiting to write until all the stockprices have been collected, a HUGE speed increase was observed.

                                    x++;
                                }
                            }
                        }
                        stock.state = "f";
                        db.SaveChanges();
                        stock = null; // Attempt to reclaim memory
                    }
                }

                log.Info("Finished the stock price import process");

            }
            catch (Exception e)
            {
                log.Error("Error loading stock prices :" + e.Message + "\n" + e.StackTrace);
            }
        }

    }
}
