﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.StockPrices.Import.Process.Loaders
{
    public interface IStockPriceLoader
    {
        void Process();
    }
}
