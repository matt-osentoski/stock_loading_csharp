﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace Trading.Stock.Import.Process.Model
{
    /// <summary>
    /// This Class is a CSV mapping class for the FileHelpers package.
    /// The original CSV files are sourced from :
    /// http://www.nasdaq.com/screening/company-list.aspx
    /// 
    /// NOTE: To use the attributes for 'QuoteMOde', the class must use fields instead of properties.
    /// </summary>
    [DelimitedRecord(",")]
    public class NasdaqComCSVMapper
    {
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string Symbol;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string Name;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string LastSale;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string MarketCap;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string IPOYear;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string Sector;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string Industry;
        [FieldQuoted(QuoteMode.AlwaysQuoted)]
        public string SummaryQuote;
    }
}
