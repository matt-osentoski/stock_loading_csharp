﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using Trading.Stock.Import.Process.Loaders;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace Trading.Stock.Import.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize the logger.
            var log = LogManager.GetLogger(typeof(Program));

            //Use unity here to instantiate the object
            var concreteLoader = ConfigurationManager.AppSettings["ConcreteStockLoaderName"];
            UnityContainer container = new UnityContainer();
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Configure(container);
            var loader = container.Resolve<IStockLoader>(concreteLoader);

            //NOTE: Instead of using unity, a simple direct concrete implementation could be used.  I'm
            //using Unity mostly to try it out and to make this process flexible with different implmentations
            //Simple instantiation Example:
            //IStockLoader loader = new NasdaqComLoader();

            loader.Process();
        }
    }
}
