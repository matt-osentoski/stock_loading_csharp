﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using System.IO;
using Trading.Data;
using Trading.Stock.Import.Process.Model;
using FileHelpers;
using System.Text.RegularExpressions;
using System.Configuration;

namespace Trading.Stock.Import.Process.Loaders
{
    public class NasdaqComLoader : IStockLoader
    {
        protected ILog log;

        public NasdaqComLoader()
        {
            log = LogManager.GetLogger(this.GetType());
        }

        public void Process()
        {
            using (var db = new TradingEntities())
            {
                try
                {
                    log.Info("Starting stock import process using CSV files from nasdaq.com");
                    var nasdaqFile = ConfigurationManager.AppSettings["NasdaqFile"];
                    var nyseFile = ConfigurationManager.AppSettings["NYSEFile"];
                    var filenames = new String[2] { nasdaqFile, nyseFile };
                    var baseDirectory = ConfigurationManager.AppSettings["StocksBaseDir"];
                    foreach (var filename in filenames)
                    {
                        string indexName = null;
                        if (filename.StartsWith("nasdaq"))
                            indexName = "Nasdaq";
                        else if (filename.StartsWith("nyse"))
                            indexName = "NYSE";

                        // Query the Database for existing stocks
                        var stocks = db.Stocks.Where(s => s.active == "1" && s.stock_index == indexName).ToList();

                        var csvFile = baseDirectory + filename;
                        log.Info("First pass, adding stocks for the following index: " + indexName);

                        // Extract values out of the CSV files.
                        DelimitedFileEngine engine = new DelimitedFileEngine(typeof(NasdaqComCSVMapper));
                        engine.Options.IgnoreFirstLines = 1;
                        var stockMaps = engine.ReadFile(csvFile) as NasdaqComCSVMapper[];
                        var i = 0;
                        foreach (var stockMap in stockMaps)
                        {
                            // Skip stocks with special characters.
                            if (!Regex.IsMatch(stockMap.Symbol, @"^[a-zA-Z]+$"))
                                continue;

                            // If the stock already exists, skip to the next stock
                            if (stocks.FirstOrDefault(s => s.symbol == stockMap.Symbol) != null)
                                continue;

                            var stock = new Trading.Data.Stock()
                            {
                                symbol = stockMap.Symbol.Replace("\"", ""),
                                name = stockMap.Name.Replace("\"", ""),
                                sector = stockMap.Sector,
                                industry = stockMap.Industry,
                                stock_index = indexName,
                                is_sp500 = "0",
                                is_sp1500 = "0",
                                is_dow_ind = "0",
                                state = "n",
                                active = "1"
                            };
                            i++;
                            db.Stocks.Add(stock);
                            db.SaveChanges();
                        }
                        log.Info("Loaded " + i + " stocks for " + indexName);

                        log.Info("Second pass, disabling stocks that have been removed from: " + indexName);
                        int x = 0;
                        foreach (var stock in stocks)
                        {
                            if (stockMaps.FirstOrDefault(s => s.Symbol == stock.symbol) == null)
                            {
                                log.Info("Disabling " + stock.symbol);
                                stock.active = "0";
                                db.SaveChanges();
                                x++;
                            }
                        }
                        log.Info("Disabled " + x + " stocks for " + indexName);
                    }
                    log.Info("Finished the stock import process.");
                }
                catch (Exception e)
                {
                    log.Error("Error loading stocks :" + e.Message + "\n" + e.StackTrace);
                }
            }
        }
    }
}
