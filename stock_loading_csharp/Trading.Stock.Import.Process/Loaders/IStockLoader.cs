﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Stock.Import.Process.Loaders
{
    public interface IStockLoader
    {
        void Process();
    }
}
