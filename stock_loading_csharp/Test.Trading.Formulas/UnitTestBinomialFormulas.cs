﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Trading.Formulas;

namespace Test.Trading.Formulas
{
    [TestClass]
    public class UnitTestBinomialFormulas
    {
        [TestMethod]
        public void TestOptionPriceCallAmericanBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPriceCallAmericanBinomial(S, K, r, sigma, time, steps);
            Assert.AreEqual(14.9505, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceCallAmericanBinomial(S, K, r, sigma, time, steps);
            Assert.AreNotEqual(14.9505, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPricePutAmericanBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPricePutAmericanBinomial(S, K, r, sigma, time, steps);
            Assert.AreEqual(6.54691, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPricePutAmericanBinomial(S, K, r, sigma, time, steps);
            Assert.AreNotEqual(6.54691, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPriceDeltaAmericanCallBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPriceDeltaAmericanCallBinomial(S, K, r, sigma, time, steps);
            Assert.AreEqual(0.699792, Math.Round(testVal, 6));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceDeltaAmericanCallBinomial(S, K, r, sigma, time, steps);
            Assert.AreNotEqual(0.699792, Math.Round(testVal, 6));
        }

        [TestMethod]
        public void TestOptionPriceDeltaAmericanPutBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPriceDeltaAmericanPutBinomial(S, K, r, sigma, time, steps);
            Assert.AreEqual(-0.387636, Math.Round(testVal, 6));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceDeltaAmericanPutBinomial(S, K, r, sigma, time, steps);
            Assert.AreNotEqual(-0.387636, Math.Round(testVal, 6));
        }

        [TestMethod]
        public void TestOptionPricePartialsAmericanCallBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            double delta;
            double gamma;
            double theta;
            double vega;
            double rho;
            BinomialFormulas.OptionPricePartialsAmericanCallBinomial(S, K, r, sigma, time, steps,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreEqual(0.699792, Math.Round(delta, 6));
            Assert.AreEqual(0.0140407, Math.Round(gamma, 7));
            Assert.AreEqual(-9.89067, Math.Round(theta, 5));
            Assert.AreEqual(34.8536, Math.Round(vega, 4));
            Assert.AreEqual(56.9652, Math.Round(rho, 4));

            //Negative test
            sigma = 0.26;
            BinomialFormulas.OptionPricePartialsAmericanCallBinomial(S, K, r, sigma, time, steps,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreNotEqual(0.699792, Math.Round(delta, 6));
            Assert.AreNotEqual(0.0140407, Math.Round(gamma, 7));
            Assert.AreNotEqual(-9.89067, Math.Round(theta, 5));
            Assert.AreNotEqual(34.8536, Math.Round(vega, 4));
            Assert.AreNotEqual(56.9652, Math.Round(rho, 4));
        }

        [TestMethod]
        public void TestOptionPricePartialsAmericanPutBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            double delta;
            double gamma;
            double theta;
            double vega;
            double rho;
            BinomialFormulas.OptionPricePartialsAmericanPutBinomial(S, K, r, sigma, time, steps,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreEqual(-0.387636, Math.Round(delta, 6));
            Assert.AreEqual(0.0209086, Math.Round(gamma, 7));
            Assert.AreEqual(-1.99027, Math.Round(theta, 5));
            Assert.AreEqual(35.3943, Math.Round(vega, 4));
            Assert.AreEqual(-21.5433, Math.Round(rho, 4));

            //Negative test
            sigma = 0.26;
            BinomialFormulas.OptionPricePartialsAmericanPutBinomial(S, K, r, sigma, time, steps,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreNotEqual(-0.387636, Math.Round(delta, 6));
            Assert.AreNotEqual(0.0209086, Math.Round(gamma, 7));
            Assert.AreNotEqual(-1.99027, Math.Round(theta, 5));
            Assert.AreNotEqual(35.3943, Math.Round(vega, 4));
            Assert.AreNotEqual(-21.5433, Math.Round(rho, 4));
        }

        [TestMethod]
        public void TestOptionPriceCallAmericanDiscreteDividendsBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var dividendTimes = new double[] { 0.25, 0.75 };
            var dividendAmounts = new double[] { 2.5, 2.5 };
            var testVal = BinomialFormulas.OptionPriceCallAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreEqual(12.0233, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceCallAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreNotEqual(12.0233, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPricePutAmericanDiscreteDividendsBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var dividendTimes = new double[] { 0.25, 0.75 };
            var dividendAmounts = new double[] { 2.5, 2.5 };
            var testVal = BinomialFormulas.OptionPricePutAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreEqual(8.11801, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPricePutAmericanDiscreteDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreNotEqual(8.11801, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPriceCallAmericanProportionalDividendsBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var dividendTimes = new double[] { 0.25, 0.75 };
            var dividendAmounts = new double[] { 0.025, 0.025 };
            var testVal = BinomialFormulas.OptionPriceCallAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreEqual(11.8604, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceCallAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreNotEqual(11.8604, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPricePutAmericanProportionalDividendsBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var dividendTimes = new double[] { 0.25, 0.75 };
            var dividendAmounts = new double[] { 0.025, 0.025 };
            var testVal = BinomialFormulas.OptionPricePutAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreEqual(7.99971, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPricePutAmericanProportionalDividendsBinomial(S, K, r, sigma,
                time, steps, dividendTimes, dividendAmounts);
            Assert.AreNotEqual(7.99971, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPriceCallAmericanBinomialPayout()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var y = 0.02;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPriceCallAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
            Assert.AreEqual(13.5926, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceCallAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
            Assert.AreNotEqual(13.5926, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPricePutAmericanBinomialPayout()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var y = 0.02;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPricePutAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
            Assert.AreEqual(6.99407, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPricePutAmericanBinomialPayout(S, K, r, y, sigma, time, steps);
            Assert.AreNotEqual(6.99407, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPriceCallEuropeanBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPriceCallEuropeanBinomial(S, K, r, sigma, time, steps);
            Assert.AreEqual(14.9505, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPriceCallEuropeanBinomial(S, K, r, sigma, time, steps);
            Assert.AreNotEqual(14.9505, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPricePutEuropeanBinomial()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 100;
            var testVal = BinomialFormulas.OptionPricePutEuropeanBinomial(S, K, r, sigma, time, steps);
            Assert.AreEqual(5.43425, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BinomialFormulas.OptionPricePutEuropeanBinomial(S, K, r, sigma, time, steps);
            Assert.AreNotEqual(5.43425, Math.Round(testVal, 5));
        }
    }
}
