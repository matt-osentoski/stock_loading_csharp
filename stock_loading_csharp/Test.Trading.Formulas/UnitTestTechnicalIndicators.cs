﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Trading.Data;
using Trading.Formulas;

namespace Test.Trading.Formulas
{
    [TestClass]
    public class UnitTestTechnicalIndicators
    {
        [TestMethod]
        public void TestGetSMA()
        {
            var priceArr = new double[] {59.375, 58.9062, 54.3125, 51, 51.5938, 52, 55.4375, 
                     52.9375, 54.5312, 56.5312, 55.3438, 55.7188, 50.1562};
            var prices = new List<StockPrice>();
            foreach(var price in priceArr)
            {
                var sp = new StockPrice();
                sp.closing_price = new Decimal(price);
                prices.Add(sp);
            }
            
            var testPrice = TechnicalIndicators.GetSMA(prices, 6, 3);
            Assert.AreEqual(Math.Round(53.01043, 5), Math.Round(testPrice, 5));
        }

        [TestMethod]
        public void TestGetEMA()
        {
            var priceArr = new double[] {64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85, 
                     62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95, 
                     58.93, 57.68, 58.82, 58.87};
            var prices = new List<StockPrice>();
            foreach (var price in priceArr)
            {
                var sp = new StockPrice();
                sp.closing_price = new Decimal(price);
                prices.Add(sp);
            }
            List<double> emas = new List<double>();
            double prevEma = 0;
            for (var x = 0; x < prices.Count; x++)
            {
                var tmpEma = TechnicalIndicators.GetEMA(prices, x, 10, prevEma);
                prevEma = tmpEma;
                emas.Add(tmpEma);
            }
            Assert.AreEqual(Math.Round(61.755, 3), Math.Round(emas.ElementAt(14), 3));
        }

        [TestMethod]
        public void TestGetMACD()
        {
            double ema12 = 29.24F;
            double ema26 = 28.21F;
            double macd = TechnicalIndicators.GetMACD(ema12, ema26);
            Assert.AreEqual(Math.Round(1.03, 2), Math.Round(macd, 2));
        }

        [TestMethod]
        public void TestGetMACDEMA()
        {
            var priceArr = new double[] {64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85, 
                     62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95, 
                     58.93, 57.68, 58.82, 58.87};
            var prices = new List<StockPrice>();
            foreach (var price in priceArr)
            {
                var sp = new StockPrice();
                sp.macd_12_26 = new Decimal(price);
                prices.Add(sp);
            }
            List<double> signals = new List<double>();
            double prevSignal = 0;
            for (var x = 0; x < prices.Count; x++)
            {
                var tmpSignal = TechnicalIndicators.GetMACDEMA(prices, x, 9, prevSignal);
                prevSignal = tmpSignal;
                signals.Add(tmpSignal);
            }
            Assert.AreEqual(Math.Round(61.608, 3), Math.Round(signals.ElementAt(14), 3));
        }

        [TestMethod]
        public void TestGetMACDHistogram()
        {
            double macd = 1.01F;
            double ema9 = 0.88F;
            double macdHist = TechnicalIndicators.GetMACDHistogram(macd, ema9);
            Assert.AreEqual(Math.Round(0.13, 2), Math.Round(macdHist, 2));
        }

        [TestMethod]
        public void TestGetVolSMA()
        {
            var volArr = new int[] {2981600, 2346400, 5444000, 4773600, 7429600, 8826400,
                   6886400, 3495200, 3816000, 2387200, 3591200, 2833600,
                   5977600, 3987200, 3796000, 8344800, 3521600};
            var prices = new List<StockPrice>();
            foreach (var vol in volArr)
            {
                var sp = new StockPrice();
                sp.volume = vol;
                prices.Add(sp);
            }
            var testPrice = TechnicalIndicators.GetVolSMA(prices, 6, 3);
            Assert.AreEqual(7714133, testPrice);
        }

        [TestMethod]
        public void TestGetPPO()
        {
            var ema12 = 227.69F;
            var ema26 = 221.28F;
            var ppo = TechnicalIndicators.GetPPO(ema12, ema26);
            Assert.AreEqual(0.02897, Math.Round(ppo, 5));
        }

        [TestMethod]
        public void TestGetPPOEMA()
        {
            var priceArr = new double[] {64.75, 63.79, 63.73, 63.73, 63.55, 63.19, 63.91, 63.85, 
                     62.95, 63.37, 61.33, 61.51, 61.87, 60.25, 59.35, 59.95, 
                     58.93, 57.68, 58.82, 58.87};
            var prices = new List<StockPrice>();
            foreach (var price in priceArr)
            {
                var sp = new StockPrice();
                sp.ppo_12_26 = new Decimal(price);
                prices.Add(sp);
            }
            List<double> signals = new List<double>();
            double prevPPO = 0;
            for (var x = 0; x < prices.Count; x++)
            {
                var tmpSignal = TechnicalIndicators.GetPPOEMA(prices, x, 9, prevPPO);
                prevPPO = tmpSignal;
                signals.Add(tmpSignal);
            }
            Assert.AreEqual(Math.Round(61.608, 3), Math.Round(signals.ElementAt(14), 3));
        }

        [TestMethod]
        public void TestGetPPOHistogram()
        {
            var ppo = 0.03158;
            var ema9 = 0.02832;
            ppo = TechnicalIndicators.GetPPOHistogram(ppo, ema9);
            Assert.AreEqual(0.00326, Math.Round(ppo, 5));
        }

        [TestMethod]
        public void TestGetRSI()
        {
            var priceArr = new double[] {46.1250, 47.1250, 46.4375, 46.9375, 44.9375, 44.2500, 
                     44.6250, 45.7500, 47.8125, 47.5625, 47.0000, 44.5625, 
                     46.3125, 47.6875, 46.6875, 45.6875, 43.0625, 43.5625, 
                     44.8750, 43.6875};
            var prices = new List<StockPrice>();
            foreach (var price in priceArr)
            {
                var sp = new StockPrice();
                sp.closing_price = new Decimal(price);
                prices.Add(sp);
            }
            var rsi = TechnicalIndicators.GetRSI(prices, 19);
            Assert.AreEqual(43.9921, Math.Round(rsi, 4));
        }

        [TestMethod]
        public void TestGetStochastics()
        {
            var highArr = new double[] {421.89, 421.4, 421.62, 418.85, 420.35, 414.85, 411.64, 
                413.61, 415.83, 414.95, 415.29, 416.07, 418.28, 420.31, 
                418.62, 417.18, 416.44, 420.52, 420.58, 425.27, 425.22, 
                422.44, 421.43};
            var lowArr = new double[] {419.44, 419.78, 418.19, 416.93, 413.58, 410.7, 408.3, 
                410.53, 413.51, 413.38, 413.76, 413.35, 415.31, 417.49, 
                416.76, 414.3, 414.44, 416.34, 419.13, 419.58, 419.54, 
                417.77, 419.62};
            var closeArr = new double[] {420.74, 421.34, 418.19, 418.26, 414.85, 410.72, 411.61, 
                413.51, 413.53, 414.84, 414.03, 416.07, 417.98, 417.98, 
                417.08, 414.44, 416.36, 419.95, 419.58, 425.27, 419.77, 
                419.92, 419.93};
            var prices = new List<StockPrice>();
            for (int x = 0; x<highArr.Count(); x++)
            {
                var sp = new StockPrice()
                {
                    high_price = new Decimal(highArr[x]),
                    low_price = new Decimal(lowArr[x]),
                    closing_price = new Decimal(closeArr[x]),
                };
                prices.Add(sp);
            }
            double k, d, kSlow, dSlow = 0;
            TechnicalIndicators.GetStochastics(prices, 22, 10, 3, out k, out d, out kSlow, out dSlow);
            Assert.AreEqual(51.32, Math.Round(k, 2));
            Assert.AreEqual(52.14, Math.Round(d, 2));
            Assert.AreEqual(52.14, Math.Round(kSlow, 2));
            Assert.AreEqual(66.84, Math.Round(dSlow, 2));
        }

        [TestMethod]
        public void TestGetATR()
        {
            var highArr = new double[] {61, 61, 58.8438, 55.125, 54.0625, 53.9688, 56, 55.2188, 
                    55.0312, 57.4922, 57.0938, 56.8125, 55.5625, 50.0625, 
                    47.6875, 44.9062, 47.375, 47.625, 48.0156, 45.0391, 43.75, 
                    43.25, 43, 42.5, 44.875, 44.3125, 41.2188, 39.375, 40.9375, 
                    40.5938, 46, 48.125, 45};
            var lowArr = new double[] {59.0312, 58.375, 53.625, 47.4375, 50.5, 49.7812, 52.5, 
                   52.625, 53.25, 53.75, 55.25, 54.3438, 50, 46.8438, 44.4688, 
                   40.625, 44.1641, 45.125, 43.25, 42.6875, 40.75, 39.9688, 40, 
                   40.75, 43.375, 40.0625, 37.625, 36.5, 37.5625, 36.9375, 
                   40.8438, 42.5625, 42.5};
            var closeArr = new double[] {59.375, 58.9062, 54.3125, 51, 51.5938, 52, 55.4375, 
                     52.9375, 54.5312, 56.5312, 55.3438, 55.7188, 50.1562, 
                     48.8125, 44.5938, 42.6562, 47, 46.9688, 43.625, 44.6562, 
                     40.8125, 42.5625, 40, 42.4375, 44.0938, 40.625, 39.875, 
                     38.0312, 38.4688, 39.4375, 45.875, 44.25, 42.8125};
            var prices = new List<StockPrice>();

            for (int x = 0; x<highArr.Count(); x++)
            {
                var sp = new StockPrice()
                {
                    high_price = new Decimal(highArr[x]),
                    low_price = new Decimal(lowArr[x]),
                    closing_price = new Decimal(closeArr[x]),
                };
                prices.Add(sp);
            }
            
            double prevAtr = 0;
            var atrResults = new List<double>();
            for (int x = 0; x<prices.Count; x++)
            {
                var atr = TechnicalIndicators.GetATR(prices, x, prevAtr);
                prevAtr = atr;
                atrResults.Add(atr);
            }
            Assert.AreEqual(3.77148, Math.Round(atrResults.ElementAt(32), 5));
        }

        [TestMethod]
        public void TestGetHistoricalVolatility()
        {
            var closeArr = new double[] {31.65, 31.97, 32.25, 32.28, 34.62,
                34.48, 32.28, 32.73, 34.9, 35.1, 35.33, 34.94, 35.23,
                35.24, 35.38, 36.03, 36.12, 36.32, 36.99, 38.45, 38.76,
                39.81, 38.9, 39.42, 39.47, 40.45, 39.37, 39.18, 40.6, 42.31};
            var prices = new List<StockPrice>();
            for (int x = 0; x < closeArr.Count(); x++)
            {
                var sp = new StockPrice()
                {
                    closing_price = new Decimal(closeArr[x])
                };
                prices.Add(sp);
            }
            var vola = TechnicalIndicators.GetHistoricalVolatility(prices, 28, 10, 252);
            Assert.AreEqual(0.364606, Math.Round(vola, 6));
        }

        [TestMethod]
        public void TestGetEWMAVolatility()
        {
            var closeArr = new double[] {100.0, 105.0, 110.0, 100.0, 110.0, 120.0, 100.0, 125.0, 
                     90.0, 130.0, 85.0, 140.0};
            var prices = new List<StockPrice>();
            for (int x = 0; x < closeArr.Count(); x++)
            {
                var sp = new StockPrice()
                {
                    closing_price = new Decimal(closeArr[x])
                };
                prices.Add(sp);
            }
            var vola = TechnicalIndicators.GetEWMAVolatility(prices, 11, 1, 0.90, 252);
            Assert.AreEqual(0.21523, Math.Round(vola, 5));
        }

        [TestMethod]
        public void TestGetGarch1_1Volatility()
        {
            var closeArr = new double[] {100.0, 105.0, 110.0, 100.0, 110.0, 120.0, 100.0, 125.0, 
                     90.0, 130.0, 85.0, 140.0};
            var prices = new List<StockPrice>();
            for (int x = 0; x < closeArr.Count(); x++)
            {
                var sp = new StockPrice()
                {
                    closing_price = new Decimal(closeArr[x])
                };
                prices.Add(sp);
            }
            var vola = TechnicalIndicators.GetGarch1_1Volatility(prices, 11, 0.05, 0.10, 0.80, 0.90, 1, 252);
            Assert.AreEqual(0.21951, Math.Round(vola, 5));
        }
    }
}
