﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Trading.Formulas;

namespace Test.Trading.Formulas
{
    [TestClass]
    public class UnitTestBlackScholesFormulas
    {
        [TestMethod]
        public void TestNormDist()
        {
            var testVal = BlackScholesFormulas.NormDist(1.23);
            Assert.AreEqual(0.187235, Math.Round(testVal, 6));

            //Negative test
            testVal = BlackScholesFormulas.NormDist(1.24);
            Assert.AreNotEqual(0.187235, Math.Round(testVal, 6));
        }

        [TestMethod]
        public void TestCumulativeNormDist()
        {
            var testVal = BlackScholesFormulas.CumulativeNormDist(1.23);
            Assert.AreEqual(0.890651, Math.Round(testVal, 6));

            //Negative test
            testVal = BlackScholesFormulas.CumulativeNormDist(1.24);
            Assert.AreNotEqual(0.890651, Math.Round(testVal, 6));
        }

        [TestMethod]
        public void TestOptionPriceCallBlackScholes()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var sigma = 0.30;
            var time = 0.50;
            var testVal = BlackScholesFormulas.OptionPriceCallBlackScholes(S, K, r, sigma, time);
            Assert.AreEqual(5.45325, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.31;
            testVal = BlackScholesFormulas.OptionPriceCallBlackScholes(S, K, r, sigma, time);
            Assert.AreNotEqual(5.45325, Math.Round(testVal, 5));
        }
        
        [TestMethod]
        public void TestOptionPricePutBlackScholes()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var sigma = 0.30;
            var time = 0.50;
            var testVal = BlackScholesFormulas.OptionPricePutBlackScholes(S, K, r, sigma, time);
            Assert.AreEqual(3.01472, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.31;
            testVal = BlackScholesFormulas.OptionPricePutBlackScholes(S, K, r, sigma, time);
            Assert.AreNotEqual(3.01472, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPriceDeltaCallBlackScholes()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var sigma = 0.30;
            var time = 0.50;
            var testVal = BlackScholesFormulas.OptionPriceDeltaCallBlackScholes(S, K, r, sigma, time);
            Assert.AreEqual(0.633737, Math.Round(testVal, 6));

            //Negative test
            sigma = 0.31;
            testVal = BlackScholesFormulas.OptionPriceDeltaCallBlackScholes(S, K, r, sigma, time);
            Assert.AreNotEqual(0.633737, Math.Round(testVal, 6));
        }

        [TestMethod]
        public void TestOptionPriceDeltaPutBlackScholes()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var sigma = 0.30;
            var time = 0.50;
            var testVal = BlackScholesFormulas.OptionPriceDeltaPutBlackScholes(S, K, r, sigma, time);
            Assert.AreEqual(-0.366263, Math.Round(testVal, 6));

            //Negative test
            sigma = 0.31;
            testVal = BlackScholesFormulas.OptionPriceDeltaPutBlackScholes(S, K, r, sigma, time);
            Assert.AreNotEqual(-0.366263, Math.Round(testVal, 6));
        }
 
        [TestMethod]
        public void TestOptionPriceImpliedVolatilityCallBlackScholesBisections()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var optionPrice = 2.5;
            var time = 0.50;
            var testVal = BlackScholesFormulas.OptionPriceImpliedVolatilityCallBlackScholesBisections(S, K, r, time, optionPrice);
            Assert.AreEqual(0.0500419, Math.Round(testVal, 7));

            //Negative test
            optionPrice = 2.6;
            testVal = BlackScholesFormulas.OptionPriceImpliedVolatilityCallBlackScholesBisections(S, K, r, time, optionPrice);
            Assert.AreNotEqual(0.0500419, Math.Round(testVal, 7));
        }

        [TestMethod]
        public void TestOptionPriceImpliedVolatilityCallBlackScholesNewton()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var optionPrice = 2.5;
            var time = 0.50;
            var testVal = BlackScholesFormulas.OptionPriceImpliedVolatilityCallBlackScholesNewton(S, K, r, time, optionPrice);
            Assert.AreEqual(0.0500427, Math.Round(testVal, 7));

            //Negative test
            optionPrice = 2.6;
            testVal = BlackScholesFormulas.OptionPriceImpliedVolatilityCallBlackScholesNewton(S, K, r, time, optionPrice);
            Assert.AreNotEqual(0.0500427, Math.Round(testVal, 7));
        }    

        [TestMethod]
        public void TestOptionPricePartialsCallBlackScholes()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var sigma = 0.30;
            var time = 0.50;
            double delta;
            double gamma;
            double theta;
            double vega;
            double rho;
            BlackScholesFormulas.OptionPricePartialsCallBlackScholes(S, K, r, sigma, time, 
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreEqual(0.633737, Math.Round(delta, 6));
            Assert.AreEqual(0.0354789, Math.Round(gamma, 7));
            Assert.AreEqual(-6.61473, Math.Round(theta, 5));
            Assert.AreEqual(13.3046, Math.Round(vega, 4));
            Assert.AreEqual(13.1168, Math.Round(rho, 4));

            //Negative test
            sigma = 0.31;
            BlackScholesFormulas.OptionPricePartialsCallBlackScholes(S, K, r, sigma, time,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreNotEqual(0.633737, Math.Round(delta, 6));
            Assert.AreNotEqual(0.0354789, Math.Round(gamma, 7));
            Assert.AreNotEqual(-6.61473, Math.Round(theta, 5));
            Assert.AreNotEqual(13.3046, Math.Round(vega, 4));
            Assert.AreNotEqual(13.1168, Math.Round(rho, 4));
        }

        [TestMethod]
        public void TestOptionPricePartialsPutBlackScholes()
        {
            var S = 50;
            var K = 50;
            var r = 0.10;
            var sigma = 0.30;
            var time = 0.50;
            double delta;
            double gamma;
            double theta;
            double vega;
            double rho;
            BlackScholesFormulas.OptionPricePartialsPutBlackScholes(S, K, r, sigma, time,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreEqual(-0.366263, Math.Round(delta, 6));
            Assert.AreEqual(0.0354789, Math.Round(gamma, 7));
            Assert.AreEqual(-1.85859, Math.Round(theta, 5));
            Assert.AreEqual(13.3046, Math.Round(vega, 4));
            Assert.AreEqual(-10.6639, Math.Round(rho, 4));

            //Negative test
            sigma = 0.31;
            BlackScholesFormulas.OptionPricePartialsPutBlackScholes(S, K, r, sigma, time,
                out delta, out gamma, out theta, out vega, out rho);
            Assert.AreNotEqual(-0.366263, Math.Round(delta, 6));
            Assert.AreNotEqual(0.0354789, Math.Round(gamma, 7));
            Assert.AreNotEqual(-1.85859, Math.Round(theta, 5));
            Assert.AreNotEqual(13.3046, Math.Round(vega, 4));
            Assert.AreNotEqual(-10.6639, Math.Round(rho, 4));
        }            
        
        [TestMethod]
        public void TestOptionPriceEuropeanCallPayout()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var q = 0.05;
            var sigma = 0.25;
            var time = 1.0;
            var testVal = BlackScholesFormulas.OptionPriceEuropeanCallPayout(S, K, r, q, sigma, time);
            Assert.AreEqual(11.7344, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BlackScholesFormulas.OptionPriceEuropeanCallPayout(S, K, r, q, sigma, time);
            Assert.AreNotEqual(11.7344, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPriceEuropeanPutPayout()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var q = 0.05;
            var sigma = 0.25;
            var time = 1.0;
            var testVal = BlackScholesFormulas.OptionPriceEuropeanPutPayout(S, K, r, q, sigma, time);
            Assert.AreEqual(7.09515, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BlackScholesFormulas.OptionPriceEuropeanPutPayout(S, K, r, q, sigma, time);
            Assert.AreNotEqual(7.09515, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPriceEuropeanCallDividends()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var dividendTimes = new double[] { 0.25, 0.75 };
            var dividendAmounts = new double[] { 2.5, 2.5 };
            var testVal = BlackScholesFormulas.OptionPriceEuropeanCallDividends(S, K, r, sigma, 
                time, dividendTimes, dividendAmounts);
            Assert.AreEqual(11.8094, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BlackScholesFormulas.OptionPriceEuropeanCallDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
            Assert.AreNotEqual(11.8094, Math.Round(testVal, 4));
        }

        [TestMethod]
        public void TestOptionPriceEuropeanPutDividends()
        {
            var S = 100;
            var K = 100;
            var r = 0.10;
            var sigma = 0.25;
            var time = 1.0;
            var dividendTimes = new double[] { 0.25, 0.75 };
            var dividendAmounts = new double[] { 2.5, 2.5 };
            var testVal = BlackScholesFormulas.OptionPriceEuropeanPutDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
            Assert.AreEqual(7.05077, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BlackScholesFormulas.OptionPriceEuropeanPutDividends(S, K, r, sigma,
                time, dividendTimes, dividendAmounts);
            Assert.AreNotEqual(7.05077, Math.Round(testVal, 5));
        }
    }
}
