﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Trading.Formulas;

namespace Test.Trading.Formulas
{
    [TestClass]
    public class UnitTestBermudanFormulas
    {
        [TestMethod]
        public void TestOptionPriceCallBermudanBinomial()
        {
            var S = 80;
            var K = 100;
            var r = 0.20;
            var q = 0.0;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 500;
            var potentialExerciseTimes = new double[] { 0.25, 0.5, 0.75 };
            var testVal = BermudanFormulas.OptionPriceCallBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
            Assert.AreEqual(7.14016, Math.Round(testVal, 5));

            //Negative test
            sigma = 0.26;
            testVal = BermudanFormulas.OptionPriceCallBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
            Assert.AreNotEqual(7.14016, Math.Round(testVal, 5));
        }

        [TestMethod]
        public void TestOptionPricePutBermudanBinomial()
        {
            var S = 80;
            var K = 100;
            var r = 0.20;
            var q = 0.0;
            var sigma = 0.25;
            var time = 1.0;
            var steps = 500;
            var potentialExerciseTimes = new double[] { 0.25, 0.5, 0.75 };
            var testVal = BermudanFormulas.OptionPricePutBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
            Assert.AreEqual(15.8869, Math.Round(testVal, 4));

            //Negative test
            sigma = 0.26;
            testVal = BermudanFormulas.OptionPricePutBermudanBinomial(S, K, r, q, sigma,
                time, potentialExerciseTimes, steps);
            Assert.AreNotEqual(15.8869, Math.Round(testVal, 4));
        }
    }
}
