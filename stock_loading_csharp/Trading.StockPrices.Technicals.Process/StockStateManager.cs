﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Trading.Data;
using Common.Logging;

namespace Trading.StockPrices.Technicals.Process
{
    public class StockStateManager
    {
        static readonly object _locker = new object();

        public static string GetNextStock()
        {
            lock (_locker)
            {
                ILog log = LogManager.GetLogger(typeof(StockStateManager));
                using (var db = new TradingEntities())
                {
                    try
                    {
                        var stock = db.Stocks.FirstOrDefault(s => s.active == "1" && (s.state == "n" || s.state == "a"));
                        if (stock == null)
                        {
                            log.Info("No more stocks are available to process");
                            return null;
                        }
                        // Mark the stock state as processing.
                        stock.state = "p";
                        db.SaveChanges();
                        return stock.symbol;
                    }
                    catch (Exception e)
                    {
                        log.Error("Error retrieving the next stock symbol :" + e.Message + "\n" + e.StackTrace);
                    }
                }
                return null;
            }
        }
    }
}
