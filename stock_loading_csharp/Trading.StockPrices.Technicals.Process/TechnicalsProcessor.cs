﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using Trading.Data;
using Trading.Formulas;

namespace Trading.StockPrices.Technicals.Process
{
    public class TechnicalsProcessor
    {
        protected ILog log;
        private string[] periods = { "d", "w", "m" };

        public TechnicalsProcessor()
        {
            log = LogManager.GetLogger(this.GetType());
        }
        public void Process()
        {
            log.Info("Starting to update stock prices with Technical Indicators");
            string stockSymbol = null;

            while ((stockSymbol = StockStateManager.GetNextStock()) != null)
            {
                log.Info("Processing: " + stockSymbol);
                using (var db = new TradingEntities())
                {
                    try
                    { 
                        var stock = db.Stocks.FirstOrDefault(s => s.symbol == stockSymbol);

                        // Initialize variables that hold the previous EMA values
                        double prevEma5 = 0;
                        double prevEma10 = 0;
                        double prevEma20 = 0;
                        double prevEma50 = 0;
                        double prevEma100 = 0;
                        double prevEma200 = 0;
                        double prevEma12 = 0;
                        double prevEma26 = 0;
                        double prevMacd12_26Signal9 = 0;
                        double prevPpo12_26Signal9 = 0;
                        double prevAtr14 = 0;

                        // Loop by periods
                        foreach (var period in periods)
                        {
                            // Grab the prices for a specific stock during a specific time period
                            var prices = stock.StockPrices
                                .Where(p => p.period == period)
                                .OrderBy(p => p.price_date).ToList();

                            for (int x = 0; x < prices.Count; x++)
                            {
                                var stockPrice = prices.ElementAt(x);

                                // SMA's
                                stockPrice.sma_5 = new Decimal(TechnicalIndicators.GetSMA(prices, x, 5));
                                stockPrice.sma_10 = new Decimal(TechnicalIndicators.GetSMA(prices, x, 10));
                                stockPrice.sma_20 = new Decimal(TechnicalIndicators.GetSMA(prices, x, 20));
                                stockPrice.sma_50 = new Decimal(TechnicalIndicators.GetSMA(prices, x, 50));
                                stockPrice.sma_100 = new Decimal(TechnicalIndicators.GetSMA(prices, x, 100));
                                stockPrice.sma_200 = new Decimal(TechnicalIndicators.GetSMA(prices, x, 200));

                                // EMA's
                                stockPrice.ema_5 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 5, prevEma5));
                                prevEma5 = (double)stockPrice.ema_5;
                                stockPrice.ema_10 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 10, prevEma10));
                                prevEma10 = (double)stockPrice.ema_10;                    
                                stockPrice.ema_20 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 20, prevEma20));
                                prevEma20 = (double)stockPrice.ema_20;                               
                                stockPrice.ema_50 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 50, prevEma50));
                                prevEma50 = (double)stockPrice.ema_50;                               
                                stockPrice.ema_100 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 100, prevEma100));
                                prevEma100 = (double)stockPrice.ema_100;                                
                                stockPrice.ema_200 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 200, prevEma200));
                                prevEma200 = (double)stockPrice.ema_200;

                                // MACD
                                stockPrice.ema_12 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 12, prevEma12));
                                prevEma12 = (double)stockPrice.ema_12;
                                stockPrice.ema_26 = new Decimal(TechnicalIndicators.GetEMA(prices, x, 26, prevEma26));
                                prevEma26 = (double)stockPrice.ema_26;

                                if (x + 1 >= 26)
                                    stockPrice.macd_12_26 = new Decimal(TechnicalIndicators.GetMACD((double)stockPrice.ema_12, (double)stockPrice.ema_26));
                                else
                                {
                                    stockPrice.macd_12_26 = 0;
                                    stockPrice.macd_12_26_signal_9 = 0;
                                    stockPrice.macd_hist_12_26 = 0;
                                }

                                if ((x + 1) >= (26 + 9))
                                {
                                    stockPrice.macd_12_26_signal_9 = new Decimal(TechnicalIndicators.GetMACDEMA(prices, x, 9, prevMacd12_26Signal9));
                                    prevMacd12_26Signal9 = (double)stockPrice.macd_12_26_signal_9;
                                    stockPrice.macd_hist_12_26 = new Decimal(TechnicalIndicators.GetMACDHistogram((double)stockPrice.macd_12_26, (double)stockPrice.macd_12_26_signal_9));
                                }
                                else
                                {
                                    stockPrice.macd_12_26_signal_9 = 0;
                                    stockPrice.macd_hist_12_26 = 0;
                                }

                                // PPO
                                if ((x + 1) >= 26)
                                    stockPrice.ppo_12_26 = new Decimal(TechnicalIndicators.GetPPO((double)stockPrice.ema_12, (double)stockPrice.ema_26));
                                else
                                {
                                    stockPrice.ppo_12_26 = 0;
                                    stockPrice.ppo_12_26_signal_9 = 0;
                                    stockPrice.ppo_hist_12_26 = 0;
                                }

                                if ((x + 1) >= (26 + 9))
                                {
                                    stockPrice.ppo_12_26_signal_9 = new Decimal(TechnicalIndicators.GetPPOEMA(prices, x, 9, prevPpo12_26Signal9));
                                    prevPpo12_26Signal9 = (double)stockPrice.ppo_12_26_signal_9;
                                    stockPrice.ppo_hist_12_26 = new Decimal(TechnicalIndicators.GetPPOHistogram((double)stockPrice.ppo_12_26, prevPpo12_26Signal9));
                                }
                                else
                                {
                                    stockPrice.ppo_12_26_signal_9 = 0;
                                    stockPrice.ppo_hist_12_26 = 0;
                                }

                                // RSI
                                stockPrice.rsi_14 = new Decimal(TechnicalIndicators.GetRSI(prices, x, 14));
                                stockPrice.rsi_20 = new Decimal(TechnicalIndicators.GetRSI(prices, x, 20));
                                stockPrice.rsi_30 = new Decimal(TechnicalIndicators.GetRSI(prices, x, 30));

                                // Stochastics 10,3
                                double stoch_fast_k_10_3 = 0;
                                double stoch_fast_d_10_3 = 0;
                                double stoch_slow_k_10_3 = 0;
                                double stoch_slow_d_10_3 = 0;
                                TechnicalIndicators.GetStochastics(prices, x, 10, 3, 
                                    out stoch_fast_k_10_3, out stoch_fast_d_10_3, out stoch_slow_k_10_3, out stoch_slow_d_10_3);
                                stockPrice.stoch_fast_k_10_3 = new Decimal(stoch_fast_k_10_3);
                                stockPrice.stoch_fast_d_10_3 = new Decimal(stoch_fast_d_10_3);
                                stockPrice.stoch_slow_k_10_3 = new Decimal(stoch_slow_k_10_3);
                                stockPrice.stoch_slow_d_10_3 = new Decimal(stoch_slow_d_10_3);

                                // Stochastics 14,3
                                double stoch_fast_k_14_3 = 0;
                                double stoch_fast_d_14_3 = 0;
                                double stoch_slow_k_14_3 = 0;
                                double stoch_slow_d_14_3 = 0;
                                TechnicalIndicators.GetStochastics(prices, x, 14, 3,
                                    out stoch_fast_k_14_3, out stoch_fast_d_14_3, out stoch_slow_k_14_3, out stoch_slow_d_14_3);
                                stockPrice.stoch_fast_k_14_3 = new Decimal(stoch_fast_k_14_3);
                                stockPrice.stoch_fast_d_14_3 = new Decimal(stoch_fast_d_14_3);
                                stockPrice.stoch_slow_k_14_3 = new Decimal(stoch_slow_k_14_3);
                                stockPrice.stoch_slow_d_14_3 = new Decimal(stoch_slow_d_14_3);

                                // Volume SMA
                                stockPrice.vol_sma_5 = TechnicalIndicators.GetVolSMA(prices, x, 5);
                                stockPrice.vol_sma_10 = TechnicalIndicators.GetVolSMA(prices, x, 10);
                                stockPrice.vol_sma_20 = TechnicalIndicators.GetVolSMA(prices, x, 20);
                                stockPrice.vol_sma_50 = TechnicalIndicators.GetVolSMA(prices, x, 50);
                                stockPrice.vol_sma_100 = TechnicalIndicators.GetVolSMA(prices, x, 100);
                                stockPrice.vol_sma_200 = TechnicalIndicators.GetVolSMA(prices, x, 200);

                                // ATR 14
                                stockPrice.atr_14 = new Decimal(TechnicalIndicators.GetATR(prices, x, prevAtr14, 14));
                                prevAtr14 = (double)stockPrice.atr_14;

                                // Historical Volatility (Simple) 20 day period, 252 trading days in a year
                                try
                                {
                                    if (period == "d")  // Only for daily charts
                                        stockPrice.hist_vola_20_252 = new Decimal(TechnicalIndicators.GetHistoricalVolatility(prices, x, 20, 252));
                                }
                                catch (OverflowException ofe)
                                {
                                    log.Info("An overflow exception occurred while calculating the Historical Volatility: " + ofe.Message);
                                    stockPrice.hist_vola_20_252 = 0;
                                }

                                // Update the stockPrice in batches to improve performance
                                if (x % 500 == 0 && x != 0)
                                    db.SaveChanges();  // By waiting to write until all the stockprices have been collected, a HUGE speed increase was observed.
                            }
                            db.SaveChanges(); // save any remaining rows that were saved in the batch within the loop
                        }
                        // Change the status of the stock to show that it has been processed
                        stock.state = "f";
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        log.Error("Error updating " + stockSymbol + " with technical indicators :" + e.Message + "\n" + e.StackTrace);
                    }
                }
            }

            log.Info("Finished updating stock prices with Technical Indicators");
        }
    }
}
