﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using System.Configuration;
using System.Threading.Tasks;
using System.Threading;

namespace Trading.StockPrices.Technicals.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize the logger.
            var log = LogManager.GetLogger(typeof(Program));
            
            // Use multi-threading to offset the loading.  This operation is CPU and DB/Write heavy. 
            var numThreads = 1;
            var threadCount = ConfigurationManager.AppSettings["ThreadCount"];
            int.TryParse(threadCount, out numThreads);
            Task[] taskArray = new Task[numThreads];
            for (var x = 0; x < taskArray.Length; x++ )
            {
                taskArray[x] = new Task((obj) =>
                {
                    var loader = new TechnicalsProcessor();
                    log.Info("Starting thread number: " + Thread.CurrentThread.ManagedThreadId);
                    loader.Process(); // Update stock prices with Technical indicators.
                }, TaskCreationOptions.LongRunning);
                taskArray[x].Start();
            }

            // IMPORTANT, you MUST wait for the tasks to finish or there's no guarantee that they will
            // finish before the app closes
            Task.WaitAll(taskArray);
        }
    }
}
