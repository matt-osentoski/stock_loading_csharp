﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Trading.Treasury.Import.Process.Loaders
{
    public interface ITreasuryLoader
    {
        void Process();
    }
}
