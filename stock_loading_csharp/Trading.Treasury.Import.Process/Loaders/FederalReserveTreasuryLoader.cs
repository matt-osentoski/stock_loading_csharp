﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using System.Configuration;
using System.Net;
using Trading.Data;
using Trading.Treasury.Import.Process.Model;
using FileHelpers;

namespace Trading.Treasury.Import.Process.Loaders
{
    public class FederalReserveTreasuryLoader : ITreasuryLoader
    {
        protected ILog log;

        public FederalReserveTreasuryLoader()
        {
            log = LogManager.GetLogger(this.GetType());
        }
        public void Process()
        {
            log.Info("Starting the Treasury import process");
            var treasuryURL = @"http://www.federalreserve.gov/datadownload/Output.aspx?rel=H15&series=bf17364827e38702b42a58cf8eaa3f78&lastObs=&from=&to=&filetype=csv&label=include&layout=seriescolumn&type=package";
            
            byte[] treasuryStream = null;
            using (var client = new WebClient())
            {  
                try
                {
                    treasuryStream = client.DownloadData(treasuryURL);

                }
                catch (Exception e)
                {
                    log.Error("A problem occurred downloading the CSV file:" + e.Message + "\n" + e.StackTrace);
                    throw e;
                }
            }
            string csvText = Encoding.ASCII.GetString(treasuryStream);
            DelimitedFileEngine engine = new DelimitedFileEngine(typeof(TreasuryCSVMap));
            engine.Options.IgnoreFirstLines = 6;
            TreasuryCSVMap[] treasuryRates = engine.ReadString(csvText) as TreasuryCSVMap[];

            using (var db = new TradingEntities())
            {
                try
                {
                    // First, truncate the treasuries table
                    db.Database.ExecuteSqlCommand("TRUNCATE TABLE treasuries");

                    var x = 0;
                    foreach (var treasuryRate in treasuryRates)
                    {
                        var treasuryEntity = new Trading.Data.Treasury()
                        {
                            time_period = treasuryRate.TimePeriod,
                            C1_month = ConvertToDec(treasuryRate.Rate1Month),
                            C3_month = ConvertToDec(treasuryRate.Rate3Month),
                            C6_month = ConvertToDec(treasuryRate.Rate6Month),
                            C1_year = ConvertToDec(treasuryRate.Rate1Year),
                            C2_year = ConvertToDec(treasuryRate.Rate2Year),
                            C3_year = ConvertToDec(treasuryRate.Rate3Year),
                            C5_year = ConvertToDec(treasuryRate.Rate5Year),
                            C7_year = ConvertToDec(treasuryRate.Rate7Year),
                            C10_year = ConvertToDec(treasuryRate.Rate10Year),
                            C20_year = ConvertToDec(treasuryRate.Rate20Year),
                            C30_year = ConvertToDec(treasuryRate.Rate30Year)
                        };
                        db.Treasuries.Add(treasuryEntity);

                        // To keep the process from running out of memory, save changes for every 500 rows 
                        if (x % 500 == 0 && x != 0)
                            db.SaveChanges();  // By waiting to write until a group of entries have been collected, a HUGE speed increase was observed.

                        x++;
                    }
                    db.SaveChanges(); 
                    log.Info(x + " rows were added to the treasuries table");
                }
                catch (Exception e)
                {
                    log.Error("A problem occurred writing a treasury row to the DB:" + e.Message + "\n" + e.StackTrace);
                    throw e;
                }
            }

            log.Info("The Treasury process is complete");
        }

        private decimal ConvertToDec(string value)
        {
            if (value == null || value == "ND" || value == "")
                return 0;
            decimal output = 0;
            Decimal.TryParse(value, out output);
            return output;
        }
    }
}
