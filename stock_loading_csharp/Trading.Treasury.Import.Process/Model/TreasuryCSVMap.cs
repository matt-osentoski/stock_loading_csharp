﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace Trading.Treasury.Import.Process.Model
{
    [DelimitedRecord(",")]
    public class TreasuryCSVMap
    {
        [FieldConverter(ConverterKind.Date, "yyyy-MM-dd")]
        public DateTime TimePeriod;
        public string Rate1Month;
        public string Rate3Month;
        public string Rate6Month;
        public string Rate1Year;
        public string Rate2Year;
        public string Rate3Year;
        public string Rate5Year;
        public string Rate7Year;
        public string Rate10Year;
        public string Rate20Year;
        public string Rate30Year;
    }
}
