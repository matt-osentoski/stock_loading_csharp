﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Logging;
using System.Configuration;
using Microsoft.Practices.Unity;
using Trading.Treasury.Import.Process.Loaders;
using Microsoft.Practices.Unity.Configuration;

namespace Trading.Treasury.Import.Process
{
    class Program
    {
        static void Main(string[] args)
        {
            // Initialize the logger.
            var log = LogManager.GetLogger(typeof(Program));

            //Use unity here to instantiate the object
            var concreteLoader = ConfigurationManager.AppSettings["ConcreteTreasuryLoaderName"];
            UnityContainer container = new UnityContainer();
            UnityConfigurationSection section = (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
            section.Configure(container);
            var loader = container.Resolve<ITreasuryLoader>(concreteLoader);

            //NOTE: Instead of using unity, a simple direct concrete implementation could be used.  I'm
            //using Unity mostly to try it out and to make this process flexible with different implmentations
            //Simple instantiation Example:
            //ITreasuryLoader loader = new FederalReserveTreasuryLoader();
            loader.Process();
        }
    }
}
